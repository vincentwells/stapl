#!/bin/bash

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

run_command=$1

reg="[1]"

# Sequential tests
if [[ $run_command =~ $reg ]]
then
  eval $run_command ./test_stl_view
  if test $? != 0
  then
      echo "ERROR:: while testing test_stl_view"
  fi
fi

eval $run_command ./test_domset1D 1000
if test $? != 0
then
    echo "ERROR:: while testing array_view with domset1D domain"
fi

eval $run_command ./test_domain_view
if test $? != 0
then
    echo "ERROR:: while testing test_domain_view"
fi

eval $run_command ./test_counting 1234
if test $? != 0
then
    echo "ERROR:: while testing test_counting"
fi

eval $run_command ./test_overlap_view 123 4
if test $? != 0
then
    echo "ERROR:: while testing overlap_view"
fi

eval $run_command ./test_partitioned_view 1234
if test $? != 0
then
    echo "ERROR:: while testing test_partitioned_view"
fi

eval $run_command ./test_strided_view 1000
if test $? != 0
then
    echo "ERROR:: while testing test_strided_view"
fi

eval $run_command ./test_proxy_conversion
if test $? != 0
then
    echo "ERROR:: while testing arithmetic conversion in proxy ops"
fi

eval $run_command ./test_pair_proxy 1000
if test $? != 0
then
    echo "ERROR:: while testing test_pair_proxy"
fi

eval $run_command ./test_vector_proxy 100
if test $? != 0
then
    echo "ERROR:: while testing test_vector_proxy"
fi

eval $run_command ./test_set_proxy
if test $? != 0
then
echo "ERROR:: while testing test_set_proxy"
fi


eval $run_command ./test_repeat_view
if test $? != 0
then
    echo "ERROR:: while testing test_repeat_view"
fi


if [ -n "${TBBROOT}" ]
then
    eval $run_command ./test_tbb_range_adaptor 5000
    if test $? != 0
    then
        echo "ERROR:: while testing test_tbb_range_adaptor"
    fi
fi
