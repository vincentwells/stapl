/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/views/strided_view.hpp>
#include <stapl/views/counting_view.hpp>
#include <stapl/algorithms/algorithm.hpp>
#include <stapl/algorithms/numeric.hpp>

#include <cmath>

#include "../test_report.hpp"

stapl::exit_code stapl_main(int argc, char* argv[])
{
  if (argc < 2) {
    std::cout << "usage: exe n" << std::endl;
    exit(1);
  }

  const size_t n = atoi(argv[1]);

  using std::floor;
  const size_t exp_sums[] = {
    size_t( n*(-1 + 2*n) ),
    size_t( (-1 + n)*n ),
    size_t( (3*floor((-1 + 2*n)/3.)*(1 + floor((-1 + 2*n)/3.)))/2. ),
    size_t( n*(-1 + 2*n) ),
    size_t( n*n ),
    size_t( ((1 + floor((2*(-1 + n))/3.))*(2 + 3*floor((2*(-1 + n))/3.)))/2. ),
    size_t( (-1 + n)*(1 + 2*n) ),
    size_t( (-1 + n)*n ),
    size_t( (floor((2*n)/3.)*(1 + 3*floor((2*n)/3.)))/2. ),
    size_t( (1 + n)*(-3 + 2*n) ),
    size_t( -1 + n*n ),
    size_t( (3*(1 + floor((2*(-2 + n))/3.))*(2 + floor((2*(-2 + n))/3.)))/2. )
  };

  auto counting = stapl::counting_view<size_t>(2*n);

  size_t const* exp_sum = &exp_sums[0];

  for (int start = 0; start < 4; ++start)
    for (int step = 1; step < 4; ++step)
    {
      auto sv = stapl::make_strided_view(counting, step, start);

      std::stringstream ss;
      ss << "Testing strided_view, start=" << start << ", step=" << step;
      STAPL_TEST_REPORT(stapl::accumulate(sv, 0ul) == *exp_sum++, ss.str())
    }

  STAPL_TEST_REPORT(
    stapl::accumulate(stapl::make_strided_view(counting, 2*n-1, 0), 0ul)==2*n-1,
    "Testing strided_view, start=0, step=view.size()-1")

  STAPL_TEST_REPORT(
    stapl::accumulate(stapl::make_strided_view(counting, 2*n-1, 1), 0ul) == 1,
    "Testing strided_view, start=1, step=view.size()-1")

  return EXIT_SUCCESS;
}
