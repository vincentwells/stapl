/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <vector>

#include <stapl/algorithms/algorithm.hpp>
#include <stapl/containers/array/array.hpp>
#include <stapl/views/array_view.hpp>
#include <stapl/utility/do_once.hpp>

#include "../test_report.hpp"


using namespace stapl;


struct equal_one
{
  typedef size_t result_type;

  template<typename Ref>
  size_t operator()(Ref r)
  {
    return r == 1;
  }
};


struct nested_mr
{
  typedef size_t result_type;

  template<typename VectorRef>
  size_t operator()(VectorRef& vec_ref)
  {
    return map_reduce<skeletons::tags::no_coarsening>(
      equal_one(), stapl::plus<size_t>(), make_array_view(vec_ref));
  }
};


stapl::exit_code stapl_main(int argc, char* argv[])
{
  // Testing view over std:vector

  typedef std::vector<size_t>        pa_t;
  typedef array_view<pa_t>           view_t;
  typedef view_t::domain_type        dom_t;

  pa_t pa1(100,1);

  view_t view1(pa1);
  size_t res = count(view1,size_t(1));
  STAPL_TEST_REPORT(res == 100,"Testing std::vector [0..99]");

  view_t view4(pa1,dom_t(10,89));
  res = count(view4,size_t(1));
  STAPL_TEST_REPORT(res == 80,"Testing std::vector [10..89]");


  // Testing view<proxy<vector<T>, A>> (non coarsened).

  array<pa_t> ct(10, pa_t(10, 1));

  res = map_reduce<skeletons::tags::no_coarsening>(
    nested_mr(), stapl::plus<size_t>(), make_array_view(ct));

  STAPL_TEST_REPORT(res == 100,
    "Testing view<proxy<vector<T>, A>> (non coarsened)");

  return EXIT_SUCCESS;
}
