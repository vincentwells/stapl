/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/array.hpp>
#include <stapl/utility/tuple.hpp>
#include <stapl/containers/type_traits/dimension_traits.hpp>
#include <stapl/views/array_view.hpp>

#include <boost/mpl/int.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/equal.hpp>

#include "../../test_report.hpp"

namespace stapl {

struct some_tag_class
{ };

template<>
struct dimension_traits<some_tag_class>
{
  typedef boost::mpl::int_<42> type;
};

} // end namespace stapl

using namespace stapl;

template <typename Element, typename Element2>
struct equal_to_impl
  : public std::integral_constant<
             bool,
             Element::value == Element2::value>
{ };

stapl::exit_code stapl_main(int argc, char* argv[])
{
  // create vector of dimensions for tuple types
  typedef boost::mpl::vector<
            dimension_traits<tuple<size_t> >::type,
            dimension_traits<tuple<size_t, char> >::type,
            dimension_traits<tuple<size_t, char, float> >::type
          > tuple_dimensions;

  // create vector of dimensions for various types
  typedef boost::mpl::vector<
            dimension_traits<array<int> >::type,
            dimension_traits<array_view<array<int > > >::type,
            dimension_traits<some_tag_class>::type
          > various_dimensions;

  // test tuple dimension types
  typedef boost::mpl::equal<
            boost::mpl::vector<
              boost::mpl::int_<1>,
              boost::mpl::int_<2>,
              boost::mpl::int_<3>
            >,
            tuple_dimensions,
            equal_to_impl<boost::mpl::_, boost::mpl::_>
          >::type tuple_test;

  // test various dimension types
  typedef boost::mpl::equal<
            boost::mpl::vector<
              boost::mpl::int_<1>,
              boost::mpl::int_<1>,
              boost::mpl::int_<42>
            >,
            various_dimensions,
            equal_to_impl<boost::mpl::_, boost::mpl::_>
          >::type various_test;

  BOOST_MPL_ASSERT(( tuple_test ));
  BOOST_MPL_ASSERT(( various_test ));

  STAPL_TEST_REPORT(true, "Testing dimension_traits");

  return EXIT_SUCCESS;
}
