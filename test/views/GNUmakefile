# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ifndef STAPL
  STAPL = $(shell echo "$(PWD)" | sed 's,/test/views,,')
endif

include $(STAPL)/GNUmakefile.STAPLdefaults

OBJS:= test_counting test_domset1D test_domain_view \
       test_map_proxy test_pair_proxy \
       test_overlap_view test_partitioned_domain test_partitioned_view \
       test_repeat_view test_stl_view \
       test_strided_view test_vector_proxy \
       test_set_proxy \
       test_proxy_conversion

ifdef TBBROOT
  OBJS+=test_tbb_range_adaptor
  CXXFLAGS+=-I${TBBROOT}/include -fno-inline
  LIB+=-L${TBBROOT}/lib -ltbb -ltbbmalloc
endif

.PHONY:compile test clean
default: compile

compile: $(OBJS)

test: compile
	$(call staplrun,1) ./test_stl_view
	./testme.sh "$(call staplrun,1)"
	./testme.sh "$(call staplrun,4)"

clean:
	rm -rf *.o *.i *.err *~ *.TVD.* *.o core* a.out ii_files rii_files $(OBJS)
