/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/utility/static_match.hpp>

using namespace stapl;

using std::true_type;
using std::false_type;
using std::pair;
using std::tuple;

struct foo { };
struct bar { };
struct baz { };

void complete_options()
{
  // 0 0 0  baz
  // 0 0 1  baz
  // 0 1 0  bar
  // 0 1 1  bar
  // 1 0 0  foo
  // 1 0 1  foo
  // 1 1 0  foo
  // 1 1 1  foo
  typedef tuple<
    pair<tuple<true_type,  dont_care,  dont_care>, foo>,
    pair<tuple<false_type, true_type,  dont_care>, bar>,
    pair<tuple<dont_care,  dont_care,  dont_care>, baz>
  > options;

  typedef static_match<
    tuple<true_type,  true_type,  true_type >, options>::type sm_111;
  typedef static_match<
    tuple<true_type,  true_type,  false_type>, options>::type sm_110;
  typedef static_match<
    tuple<true_type,  false_type, true_type >, options>::type sm_101;
  typedef static_match<
    tuple<true_type,  false_type, false_type>, options>::type sm_100;
  typedef static_match<
    tuple<false_type, true_type,  true_type >, options>::type sm_011;
  typedef static_match<
    tuple<false_type, true_type,  false_type>, options>::type sm_010;
  typedef static_match<
    tuple<false_type, false_type, true_type >, options>::type sm_001;
  typedef static_match<
    tuple<false_type, false_type, false_type>, options>::type sm_000;

  static_assert(std::is_same<sm_000, baz>::value, "000");
  static_assert(std::is_same<sm_001, baz>::value, "001");
  static_assert(std::is_same<sm_010, bar>::value, "010");
  static_assert(std::is_same<sm_011, bar>::value, "011");
  static_assert(std::is_same<sm_100, foo>::value, "100");
  static_assert(std::is_same<sm_101, foo>::value, "101");
  static_assert(std::is_same<sm_110, foo>::value, "110");
  static_assert(std::is_same<sm_111, foo>::value, "111");
}


void incomplete_options()
{
  // 0 0  foo
  // 0 1  bar
  // 1 0  baz
  // 1 1  -
  typedef tuple<
    pair<tuple<false_type, false_type>, foo>,
    pair<tuple<false_type, true_type >, bar>,
    pair<tuple<true_type,  false_type>, baz>
  > options;

  typedef static_match<tuple<true_type,  true_type >, options>::type sm_11;
  typedef static_match<tuple<true_type,  false_type>, options>::type sm_10;
  typedef static_match<tuple<false_type, true_type >, options>::type sm_01;
  typedef static_match<tuple<false_type, false_type>, options>::type sm_00;

  static_assert(std::is_same<sm_00, foo>::value, "00");
  static_assert(std::is_same<sm_01, bar>::value, "01");
  static_assert(std::is_same<sm_10, baz>::value, "10");
  static_assert(std::is_same<sm_11, not_matched>::value, "11");
}


stapl::exit_code stapl_main(int argc, char* argv[])
{
  complete_options();
  incomplete_options();

  return EXIT_SUCCESS;
}
