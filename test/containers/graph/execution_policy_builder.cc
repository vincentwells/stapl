/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#include <stapl/containers/graph/multidigraph.hpp>
#include <stapl/containers/graph/views/graph_view.hpp>
#include <stapl/containers/graph/algorithms/execution_policy_builder.hpp>

#include "../../expect.hpp"

using namespace stapl;

constexpr std::size_t g_kla_test_value = 5;
constexpr std::size_t g_hubs_test_value = 10;
constexpr double g_active_ratio_test_value = 0.125;
constexpr double g_active_ratio_default_value = 0.5;

struct test_policy_visitor
  : boost::static_visitor<void>
{
  bool m_custom_ratio;
  std::size_t& m_invoke_count;

  test_policy_visitor(bool custom_ratio, std::size_t& invoke_count)
    : m_custom_ratio(custom_ratio), m_invoke_count(invoke_count)
  {
  }

  void operator()(sgl::kla_policy& policy) const
  {
    stapl::tests::expect_eq(policy.k(), g_kla_test_value) << "KLA value";

    const double expected = m_custom_ratio ? g_active_ratio_test_value
                                           : g_active_ratio_default_value;
    stapl::tests::expect_eq(policy.active_ratio(), expected)
      << "KLA active ratio";

    ++m_invoke_count;
  }

  void operator()(sgl::level_sync_policy& policy) const
  {
    const double expected = m_custom_ratio ? g_active_ratio_test_value
                                           : g_active_ratio_default_value;
    stapl::tests::expect_eq(policy.active_ratio(), expected)
      << "Level sync active ratio";

    ++m_invoke_count;
  }

  void operator()(sgl::async_policy& policy) const
  {
    const double expected = m_custom_ratio ? g_active_ratio_test_value
                                           : g_active_ratio_default_value;
    stapl::tests::expect_eq(policy.active_ratio(), expected)
      << "Async active ratio";

    ++m_invoke_count;
  }

  template <typename T>
  void operator()(T&&) const
  {
    stapl::abort("Execution policy not found");
  }
};

stapl::exit_code stapl_main(int argc, char** argv)
{
  using graph_type = stapl::multidigraph<int>;
  using view_type = graph_view<graph_type>;

  graph_type g(10);
  view_type view(g);

  // Test default value for active ratio
  {
    auto p1
      = sgl::build_execution_policy(view).kla(g_kla_test_value).build();
    auto p2 = sgl::build_execution_policy(view).async().build();
    auto p3 = sgl::build_execution_policy(view).level_sync().build();

    std::size_t invoke_count = 0;

    boost::apply_visitor(test_policy_visitor(false, invoke_count), p1);
    boost::apply_visitor(test_policy_visitor(false, invoke_count), p2);
    boost::apply_visitor(test_policy_visitor(false, invoke_count), p3);

    stapl::tests::expect_eq(invoke_count, 3ul) << "Every policy tested";
  }

  // Test custon value for active ratio
  {
    auto p1 = sgl::build_execution_policy(view)
                .kla(g_kla_test_value)
                .active_ratio(g_active_ratio_test_value)
                .build();
    auto p2 = sgl::build_execution_policy(view)
                .async()
                .active_ratio(g_active_ratio_test_value)
                .build();
    auto p3 = sgl::build_execution_policy(view)
                .level_sync()
                .active_ratio(g_active_ratio_test_value)
                .build();

    std::size_t invoke_count = 0;

    boost::apply_visitor(test_policy_visitor(true, invoke_count), p1);
    boost::apply_visitor(test_policy_visitor(true, invoke_count), p2);
    boost::apply_visitor(test_policy_visitor(true, invoke_count), p3);

    stapl::tests::expect_eq(invoke_count, 3ul) << "Every policy tested";
  }

  return EXIT_SUCCESS;
}
