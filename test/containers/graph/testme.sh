#!/bin/sh

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

run_command=$1

# remove the output file from any previous runs.
rm -f graph.out

eval $run_command ./graph 100 30
if test $? != 0
then
    echo "ERROR:: while testing graph"
fi

eval $run_command ./csr 100 30
if test $? != 0
then
    echo "ERROR:: while testing csr graph"
fi

eval $run_command ./csr_equality 7 15 ./inputs/grid-8-8.el
if test $? != 0
then
    echo "ERROR:: while testing csr graph"
fi

eval $run_command ./breadth_first_search 1000 10
if test $? != 0
then
    echo "ERROR:: while testing breadth_first_search"
fi


eval $run_command ./graph_test
if test $? != 0
then
    echo "ERROR:: while testing const correctness in graph_test"
fi


eval $run_command ./nested_graph_sharder ./inputs/grid-8-8.el ./inputs/grid-8-8/grid-8-8.el.metadata
if test $? != 0
then
    echo "ERROR:: while testing nested_graph_sharder"
fi

eval $run_command ./matrix_market_reader ./inputs/grid-8-8.el ./inputs/grid-8-8.mtx
if test $? != 0
then
    echo "ERROR:: while testing matrix_market_reader"
fi

eval $run_command ./graph_sharder_test ./inputs/grid-8-8.adj inputs/grid-8-8-sharded/grid-8-8.adj
if test $? != 0
then
    echo "ERROR:: while testing graph_sharder_test"
fi

eval $run_command ./read_weighted_edge_list inputs/grid-8-8-weighted.el
if test $? != 0
then
    echo "ERROR:: while testing read_weighted_edge_list"
fi

eval $run_command ./matrix_market_writer inputs/grid-8-8.mtx market.out
diff -b inputs/grid-8-8.mtx market.out > /dev/null
if test $? != 0
then
    echo "ERROR:: while testing matrix_market_writer"
fi
rm market.out

eval $run_command ./frontier_traversal
if test $? != 0
then
    echo "ERROR:: while testing frontier traversal"
fi

eval $run_command ./sort_edges 256
if test $? != 0
then
    echo "ERROR:: while testing sort_edges"
fi

