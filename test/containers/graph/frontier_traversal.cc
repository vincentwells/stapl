/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#include <stapl/containers/graph/multidigraph.hpp>
#include <stapl/containers/graph/views/graph_view.hpp>
#include <stapl/containers/graph/algorithms/breadth_first_search.hpp>
#include <stapl/containers/graph/algorithms/execution_policy_builder.hpp>
#include <stapl/containers/graph/algorithms/properties.hpp>
#include <stapl/containers/graph/generators/cycles.hpp>

#include <stapl/containers/partitions/blocked_partition.hpp>
#include <stapl/algorithms/algorithm.hpp>

#include "../../test_report.hpp"

using namespace stapl;

struct reachability_init_op
{
  template<typename Vertex>
  bool operator()(Vertex&& v) const
  {
    if (v.descriptor() == 0) {
      v.property() = 0;
      return true;
    } else {
      v.property() = std::numeric_limits<std::size_t>::max();
      return false;
    }
  }
};

struct reachability_neighbor_op
{
  std::size_t m_level;

  template<typename Vertex>
  bool operator()(Vertex&& v) const
  {
    if (m_level >= v.property())
      return false;

    v.property() = m_level;
    return true;
  }

  void define_type(stapl::typer& t)
  {
    t.member(m_level);
  }
};

struct reachability_vertex_op
{
  template<typename Vertex, typename Visitor>
  bool operator()(Vertex&& v, Visitor&& visitor) const
  {
    visitor.visit_all_edges(v, reachability_neighbor_op{v.property() + 1});
    return true;
  }
};

struct reachability_reducer
{
  template<typename VP1, typename VP2>
  bool operator()(VP1& p1, VP2& p2) const
  {
    if (p1 > p2) {
      p1 = p2;
      return true;
    }
    return false;
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Traverse the graph from vertex 0 to compute the BFS level
//////////////////////////////////////////////////////////////////////
template <typename View, typename PolicyBuilder>
void reachable_from_zero(View vw,
                         PolicyBuilder policy_builder,
                         stapl::sgl::frontier_type frontier_type)
{
  sgl::execute(
    policy_builder
      .active_ratio(frontier_type == sgl::frontier_type::bitmap ? 0.9 : 0)
      .build(),
    vw,
    reachability_vertex_op{},
    reachability_neighbor_op{ 0 },
    reachability_init_op{},
    reachability_reducer{});
}

//////////////////////////////////////////////////////////////////////
/// @brief Predicate to compute whether or not all vertices are reached
/// at the expected level
//////////////////////////////////////////////////////////////////////
template<typename Map>
struct level_is_expected
{
  Map m_expected;

  level_is_expected(Map expected) : m_expected(std::move(expected)) { }

  template<typename Vertex>
  bool operator()(Vertex&& v) const
  {
    return v.property() == m_expected.at(v.descriptor());
  }

  void define_type(stapl::typer& t)
  {
    t.member(m_expected);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Given a graph with two disconnected components, traverse
/// it from a single vertex and ensure that all vertices in the first
/// component are reached and no vertices in the second component are
/// reached
//////////////////////////////////////////////////////////////////////
template <typename View, typename PolicyBuilder>
bool test_reachability(View vw,
                       PolicyBuilder policy_builder,
                       stapl::sgl::frontier_type frontier_type)
{
  reachable_from_zero(vw, policy_builder, frontier_type);

  using map_type = std::unordered_map<std::size_t, std::size_t>;

  // The first cycle should be visited
  map_type expected = {
    {0,0}, {1,1}, {2,2}, {3,3}, {4,4}, {5,3}, {6,2}, {7,1}
  };

  // The second cycle should be unvisited
  for (std::size_t i = 8; i < vw.size(); ++i)
    expected[i] = std::numeric_limits<std::size_t>::max();

  return stapl::all_of(vw, level_is_expected<map_type>{std::move(expected)});

}

//////////////////////////////////////////////////////////////////////
/// @brief Test all paradigms using the reachability algorithm
//////////////////////////////////////////////////////////////////////
template <typename View>
void test_reachability_paradigms(View vw,
                                 stapl::sgl::frontier_type frontier_type)
{
  const std::string frontier_string
    = frontier_type == sgl::frontier_type::vector ? "Vector" : "Bitmap";

  bool passed = test_reachability(
    vw, sgl::build_execution_policy(vw).kla(0), frontier_type);

  STAPL_TEST_REPORT(passed, frontier_string + " KLA reachability");
}

//////////////////////////////////////////////////////////////////////
/// @brief Test all paradigms and frontiers with a balanced static graph
//////////////////////////////////////////////////////////////////////
void test_balanced_graph(const std::size_t num_components,
                         const std::size_t component_size)
{
  using graph_type
    = stapl::graph<stapl::UNDIRECTED, stapl::MULTIEDGES, std::size_t>;
  using graph_view_type = stapl::graph_view<graph_type>;

  test_reachability_paradigms(
    stapl::generators::make_unconnected_cycles<graph_view_type>(num_components,
                                                                component_size),
    stapl::sgl::frontier_type::bitmap);
  test_reachability_paradigms(
    stapl::generators::make_unconnected_cycles<graph_view_type>(num_components,
                                                                component_size),
    stapl::sgl::frontier_type::vector);
}

//////////////////////////////////////////////////////////////////////
/// @brief Create a graph that has more than one base container per location
//////////////////////////////////////////////////////////////////////
stapl::
  graph_view<stapl::
               graph<stapl::DIRECTED,
                     stapl::MULTIEDGES,
                     std::size_t,
                     stapl::properties::no_property,
                     stapl::
                       block_partitioner<stapl::indexed_domain<std::size_t>>>>
  make_block_graph(const std::size_t num_components,
                   const std::size_t component_size)
{
  using domain_type = stapl::indexed_domain<std::size_t>;
  using partition_type = stapl::block_partitioner<domain_type>;

  using graph_type = stapl::graph<stapl::DIRECTED,
                                  stapl::MULTIEDGES,
                                  std::size_t,
                                  stapl::properties::no_property,
                                  partition_type>;
  using graph_view_type = stapl::graph_view<graph_type>;

  domain_type dom(0, num_components*component_size-1, true);
  partition_type part(dom, stapl::get_num_locations()*2);
  graph_type::mapper_type mapper(part.domain());

  graph_view_type vw(new graph_type(part, mapper));

  stapl::generators::make_unconnected_cycles(
    vw, num_components, component_size);

  return vw;
}

//////////////////////////////////////////////////////////////////////
/// @brief Test all paradigms and frontiers using a graph with more
/// than one base containers per location
//////////////////////////////////////////////////////////////////////
void test_block_graph(const std::size_t num_components,
                      const std::size_t component_size)
{
  test_reachability_paradigms(make_block_graph(num_components, component_size),
                              stapl::sgl::frontier_type::vector);
  test_reachability_paradigms(make_block_graph(num_components, component_size),
                              stapl::sgl::frontier_type::bitmap);
}

stapl::exit_code stapl_main(int argc, char** argv)
{
  const std::size_t num_components = 2;
  const std::size_t component_size = 8;

  test_balanced_graph(num_components, component_size);
  test_block_graph(num_components, component_size);

  return EXIT_SUCCESS;
}
