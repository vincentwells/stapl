/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/algorithms/algorithm.hpp>
#include <stapl/containers/graph/csr_graph.hpp>
#include <stapl/containers/graph/short_csr_graph.hpp>
#include <stapl/containers/graph/graph.hpp>
#include <stapl/containers/graph/views/graph_view.hpp>
#include <stapl/containers/graph/generators/mesh.hpp>
#include <stapl/containers/graph/algorithms/graph_io.hpp>

#include "test_util.h"
#include "../../test_report.hpp"

using namespace stapl;

template<typename Graph, typename CSRGraph>
void test_mesh(std::size_t x, std::size_t y)
{
  auto csr_g = generators::make_mesh<graph_view<CSRGraph>>(x, y);
  auto g = generators::make_mesh<graph_view<Graph>>(x, y);

  // All locations try do add a duplicate edge. Checking to see
  // if the non-multiedges flag appropriately handles this
  csr_g.add_edge_async({0,1});
  rmi_fence();

  csr_g.container().commit();

  bool passed = compare_graphs(csr_g, g);

  STAPL_TEST_REPORT(passed, "CSR mesh equality");
}


template<typename Graph, typename CSRGraph>
void test_edge_list(std::string const& filename)
{
  auto csr_g = read_edge_list<CSRGraph>(filename);
  auto g = read_edge_list<Graph>(filename);

  csr_g.container().commit();

  bool passed = compare_graphs(csr_g, g);

  STAPL_TEST_REPORT(passed, "CSR edge list equality");
}

stapl::exit_code stapl_main(int argc, char* argv[])
{
  if (argc < 4) {
    std::cerr << "usage: exe x y edge_list_file" << std::endl;
    exit(1);
  }

  const std::size_t x = atol(argv[1]);
  const std::size_t y = atol(argv[2]);
  const std::string filename = argv[3];

  using csr_graph_type =
    stapl::csr_graph<stapl::DIRECTED, stapl::NONMULTIEDGES>;

  using short_csr_graph_type =
    stapl::short_csr_graph<stapl::DIRECTED>;

  using graph_type = stapl::graph<stapl::DIRECTED, stapl::NONMULTIEDGES>;

  test_mesh<graph_type, csr_graph_type>(x, y);
  test_mesh<graph_type, short_csr_graph_type>(x, y);
  test_edge_list<graph_type, csr_graph_type>(filename);

  return EXIT_SUCCESS;
}
