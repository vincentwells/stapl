/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/containers/array/array.hpp>

stapl::exit_code stapl_main(int argc, char* argv[])
{
  if (argc < 2) {
    std::cerr << "usage: exe n" << std::endl;
    exit(1);
  }

  size_t sum = 0;
  size_t n = atoi(argv[1]);
  stapl::array<int> c(n);

  for (size_t i = 0; i < n; ++i)
    c.set_element(i, n-i);
  stapl::rmi_fence();

/*  for (size_t i = 0; i < n; ++i)
    sum += c.get_element(i);*/

  if (stapl::get_location_id() == 0)
    c.migrate(0, 1);
  else
    c.set_element(0, stapl::get_location_id()*1000);


  stapl::rmi_fence();

  std::cout << "sum: " << sum << std::endl;
  //std::cout << "elem 0 is on loc " << c.distribution().dir().find(0) << std::endl;

  stapl::rmi_fence();

  if (stapl::get_location_id() == 1) {
    stapl::future<int> e = c.get_element_split(0);
    std::cout << "elem 0 has value " << e.get() << std::endl;
  }

  /*if (stapl::get_location_id() == 1)
    c.migrate(0, 0);

  if (stapl::get_location_id() == 0) {
    stapl::pc_future<int> e = c.get_element_split(0);
    std::cout << "elem 0 has value " << e.get() << std::endl;
  }*/

  return EXIT_SUCCESS;
}
