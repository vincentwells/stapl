/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/paragraph/paragraph.hpp>

#include <stapl/containers/array/array.hpp>
#include <stapl/containers/partitions/blocked_partition.hpp>
#include <stapl/views/array_view.hpp>

using namespace stapl;

struct dummy_wf
{
  typedef void result_type;

  template <typename ViewA>
  void operator()(ViewA const&) const
  { }
};


stapl::exit_code stapl_main(int, char*[])
{
  typedef indexed_domain<size_t>         dom_t;
  typedef block_partitioner<dom_t>       part_t;
  typedef array<size_t, part_t>          cnt_t;
  typedef array_view<cnt_t>              vw_t;

  part_t part(dom_t(0, 159999), 100);

  cnt_t ct(part);
  vw_t  vw(ct);

  for (int i = 0; i != 100; ++i)
    map_func(dummy_wf(), vw);

  return EXIT_SUCCESS;
}
