# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ifndef STAPL
  export STAPL = $(shell echo "$(PWD)" | sed 's,/test/skeletons,,')
endif

include $(STAPL)/GNUmakefile.STAPLdefaults

TESTDIRS:=$(addsuffix .test, $(SUBDIRS))
COMPILEDIRS:=$(addsuffix .compile, $(SUBDIRS))
SUBCLEAN:= $(addsuffix .clean,$(SUBDIRS))

#CXXFLAGS += -DSKELETONS_FULL_NODE_NAME
#CXXFLAGS += -DSHOW_RESULTS
#CXXFLAGS += -DGRAPHVIZ_OUTPUT
CXXFLAGS += -DVALIDATE_RESULTS
#CXXFLAGS += -ftime-report

LIB+=-lboost_program_options

OBJS:=$(patsubst %.cc,%,$(wildcard *.cc))

.PHONY:compile test clean
default: compile

compile: $(OBJS) $(COMPILEDIRS)

test_inline_cycle_check: test_inline_cycle_check.cc
	${CC} ${STAPL_CXXFLAGS} ${CXXFLAGS} -o $@.exe $< ${STAPL_LIBRARIES} ${LIB} ${LIB_EPILOGUE}2> $@ ; true

$(TESTDIRS): %.test: compile
	$(MAKE) -C $* test

$(COMPILEDIRS): %.compile:
	$(MAKE) -C $* compile

test: compile $(TESTDIRS)
	./testme.sh "$(call staplrun,1)"
	./testme.sh "$(call staplrun,2)"
	./testme.sh "$(call staplrun,3)"
	./testme.sh "$(call staplrun,4)"

clean: $(SUBCLEAN)
	rm -rf *.o *.i *.err *~ *.TVD.* *.o core* a.out ii_files rii_files results.* *.dot $(OBJS)

$(SUBCLEAN): %.clean:
	$(MAKE) -C $* clean
