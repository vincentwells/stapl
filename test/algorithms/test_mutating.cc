/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <algorithm>
#include <functional>
#include <iterator>
#include <stapl/algorithms/algorithm.hpp>
#include <stapl/algorithms/generator.hpp>
#include "test.h"
#include <boost/function.hpp>
#include <boost/bind.hpp>

using boost::bind;
using boost::function;

using std::for_each;
using std::generate;
using std::replace;
using std::replace_if;
using std::fill;
using std::replace_copy;
using std::replace_copy_if;
using std::transform;
using std::swap_ranges;
using std::stable_partition;
using std::remove_if;
using std::remove;
using std::remove_copy;
using std::remove_copy_if;
using std::reverse;
using std::reverse_copy;
using std::unique;
using std::unique_copy;
using std::copy;
using std::copy_backward;


template <typename U>
struct seq_sum_op
{
  U val;

  seq_sum_op(U v)
    : val(v)
  {}

  template<typename T>
  void operator()(T& elem) const
  { elem = elem + val; }
};


template <typename U>
struct sum_op
{
  U m_val;

  sum_op(U v)
    : m_val(v)
  {}

  void define_type(stapl::typer& t)
  { t.member(m_val); }

  template<typename Reference>
  void operator()(Reference elem) const
  { elem = elem + m_val; }
};


template <typename T>
void test_for_each(const unsigned int n, const unsigned i,
                   traits_type const& t, char const* name,
                   std::string const& ds = "none")
{
  typedef void                     ret_type;
  typedef test_one_view<ret_type>  test_type;

  typedef sum_op<data_type>      wf1;
  typedef seq_sum_op<data_type>  wf2;

  typedef wf1 (*pfun_t)(view_type const&, wf1);
  typedef wf2 (*sfun_t)(iterator_type, iterator_type, wf2);

  wf1 wfo1(10);
  wf2 wfo2(10);

  function<ret_type (view_type const&)> pfp =
    (bind((pfun_t) for_each<view_type, wf1>, _1, wfo1));

  function<ret_type (iterator_type, iterator_type)> sfp
    (bind((sfun_t) for_each<iterator_type, wf2>, _1, _2, wfo2));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(3, 1)))
    (tres, i, pfp, sfp);

  test_report(tres);
}


// stapl::copy returns void, whereas std::copy returns iterator
template<typename T>
void test_copy(const unsigned int n, const unsigned int i,
               traits_type const& t, char const* name,
               std::string const& ds = "none")
{
  typedef void                      ret_type;
  typedef test_two_view<ret_type>   test_type;

  typedef ret_type (*pfun_t)(view_type const&, view_type const&);
  typedef iterator_type (*sfun_t)(iterator_type, iterator_type, iterator_type);

  function<ret_type (view_type const&, view_type const&)>
    pfp(bind((pfun_t) stapl::copy<view_type, view_type>, _1, _2));

  function<ret_type (iterator_type, iterator_type, iterator_type)>
    sfp(bind((sfun_t) std::copy<iterator_type,iterator_type>, _1, _2, _3));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)),
    data_descriptor<pcontainer_type, view_type, null_sequence<data_type> >(
      n_elems, t.ct_create[1], t.vw_create[1], null_sequence<data_type>()))
    (tres, i, pfp, sfp);

  test_report(tres);
}

template<typename T>
void test_copy_n(const unsigned int n, const unsigned int i,
                 traits_type const& t, char const* name,
                 std::string const& ds = "none")
{
  typedef void                      ret_type;
  typedef test_two_view<ret_type>   test_type;
  typedef const unsigned long int   size_type;

  size_type n_elems = set_data_size(n, ds, "mutating");

  typedef ret_type (*pfun_t)(view_type const&, view_type const&, size_t);
  function<ret_type (view_type const&, view_type const&)>
    pfp(bind((pfun_t) stapl::copy_n<view_type, view_type>, _1, _2, n_elems));

  typedef iterator_type (*sfun_t)(iterator_type, size_type, iterator_type);
  function<ret_type (iterator_type, iterator_type)>
    sfp(bind((sfun_t) std::copy_n<iterator_type, size_type, iterator_type>, _1, n_elems, _2));

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)),
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[1], t.vw_create[1], sequence<data_type>(1, 1)))
    (tres, i, pfp, sfp);

  test_report(tres);
}


template<typename T>
struct increment
{
  T operator()(T val) const
  { return ++val; }
};


//std::swap_ranges returns an iterator, whereas stapl::swap_ranges returns void
//////////////////////////////////////////////////////////////////////
/// @brief Test function for the swap ranges algorithm.
/// @param n The number of elements to generate.
/// @param i The number of times the test will be run.
/// @param t Traits class containing the types of the containers and views to be
///   used and the function objects to instantiate them.
//////////////////////////////////////////////////////////////////////
template<typename T>
void test_swap_ranges(const unsigned int n, const unsigned int i,
                      traits_type const& t, char const* name,
                      std::string const& ds = "none")
{
  typedef void                     ret_type;
  typedef test_two_view<ret_type>  test_type;

  typedef ret_type (*pfun_t)(view_type&, view_type&);
  function<ret_type (view_type&, view_type&)>
    pfp(bind((pfun_t) stapl::swap_ranges<view_type>, _1, _2));

  typedef iterator_type (*sfun_t)(iterator_type, iterator_type, iterator_type);
  function<ret_type (iterator_type, iterator_type, iterator_type)>
    sfp(bind((sfun_t) std::swap_ranges<iterator_type, iterator_type>, _1, _2, _3));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 2)),
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[1], t.vw_create[1], sequence<data_type>(2, 2)))
    (tres, i, pfp, sfp);

  test_report(tres);
}


template<typename T>
void test_transform(const unsigned int n, const unsigned int i,
                    traits_type const& t, char const* name,
                    std::string const& ds = "none")
{
  typedef void                      ret_type;
  typedef test_two_view<ret_type>   test_type1;
  typedef test_three_view<ret_type> test_type2;
  typedef increment<data_type>      unary_fun;
  typedef std::plus<data_type>      binary_fun;

  typedef iterator_type (*sfun1_t)(iterator_type, iterator_type, iterator_type,
                                   unary_fun);
  function< void (iterator_type, iterator_type, iterator_type)>
    sfp1(bind((sfun1_t)std::transform<iterator_type, iterator_type, unary_fun>,
              _1, _2, _3, unary_fun()));

  typedef ret_type (*pfun1_t)(view_type const&, view_type const&, unary_fun);
  function<ret_type (view_type const&, view_type const&)>
    pfp1(bind((pfun1_t)stapl::transform<view_type, view_type, unary_fun>,
              _1, _2, unary_fun()));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres1(name);
  test_type1(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)),
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[1], t.vw_create[1], sequence<data_type>(1, 1)))
    (tres1, i, pfp1, sfp1);

  typedef ret_type (*pfun2_t)(view_type&, view_type&, view_type&, binary_fun);
  function<ret_type (view_type &, view_type &, view_type &)>
    pfp2(bind((pfun2_t)stapl::transform<view_type, view_type, view_type,
                                        binary_fun>,
              _1, _2, _3, binary_fun()));

  typedef iterator_type (*sfun2_t)(iterator_type, iterator_type, iterator_type,
                                   iterator_type, binary_fun);
  function< iterator_type (iterator_type, iterator_type, iterator_type,
                           iterator_type)>
    sfp2(bind((sfun2_t)std::transform<iterator_type, iterator_type,
                                      iterator_type, binary_fun>,
              _1, _2, _3, _4, binary_fun()));

  std::stringstream fun_name;
  fun_name << name << "-op";
  timed_test_result tres2(fun_name.str().c_str());
  test_type2(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)),
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[1], t.vw_create[1], sequence<data_type>(1, 1)),
    data_descriptor<pcontainer_type, view_type, null_sequence<data_type> >(
      n_elems, t.ct_create[2], t.vw_create[2], null_sequence<data_type>()))
    (tres2, i, pfp2, sfp2);

  test_report(tres1);
  test_report(tres2);
}


template<typename T>
void test_replace(const unsigned int n, const unsigned int i,
                  traits_type const& t, char const* name,
                  std::string const& ds = "none")
{
  typedef void                    ret_type;
  typedef test_one_view<ret_type> test_type;
  typedef view_type::value_type data_type;

  const data_type init1 = 10;
  const data_type init2 = 99;

  typedef void (*pfun_t)(view_type&, data_type const&, data_type const&);
  function<void (view_type&)>
    pfp(bind((pfun_t)stapl::replace<view_type>, _1, init1, init2));

  typedef void (*sfun_t)(iterator_type, iterator_type, data_type const&,
                         data_type const&);
  function<void (iterator_type, iterator_type)>
    sfp(bind((sfun_t)std::replace<iterator_type, data_type>,
             _1, _2, init1, init2));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(0, 1)))
    (tres, i, pfp, sfp);

  test_report(tres);
}


template<typename T>
void test_replace_copy(const unsigned int n, const unsigned int i,
                       traits_type const& t, char const* name,
                       std::string const& ds = "none")
{
  typedef void                    ret_type;
  typedef test_two_view<ret_type> test_type;

  const data_type int1 = 10;
  const data_type int2 = 99;

  typedef view_type::iterator (*pfun_t)(view_type&, view_type&, data_type,
                                        data_type);

  function<ret_type (view_type&, view_type&)>
    pfp( bind((pfun_t) stapl::replace_copy<view_type, view_type>,
                       _1, _2, int1, int2));

  typedef iterator_type (*sfun_t)(iterator_type, iterator_type, iterator_type,
                                  data_type const&, data_type const&);

  function<ret_type (iterator_type, iterator_type, iterator_type)>
    sfp( bind((sfun_t) std::replace_copy<iterator_type, iterator_type,
                                         data_type>,
                                         _1, _2, _3, int1, int2));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(0, 1)),
    data_descriptor<pcontainer_type, view_type, null_sequence<data_type> >(
      n_elems, t.ct_create[1], t.vw_create[1], null_sequence<data_type>()))
    (tres, i, pfp, sfp);

  test_report(tres);
}

template<typename T>
void test_replace_if(const unsigned int n, const unsigned int i,
                     traits_type const& t, char const* name,
                     std::string const& ds = "none")
{
  typedef void                         ret_type;
  typedef test_one_view<ret_type>      test_type;
  typedef view_type::value_type data_type;

  typedef stapl::less<data_type>       p_fun;
  typedef stapl::binder2nd<p_fun,data_type,false>      p_pred;

  typedef std::less<data_type>         fun;
  typedef stapl::binder2nd<fun,data_type,false>          pred;

  const data_type init1 = 10;
  const data_type init2 = 99;

  typedef ret_type (*pfun_t)(view_type &, p_pred, data_type const&);
  function<ret_type (view_type &)>
    pfp(bind((pfun_t) replace_if<view_type, p_pred>,
             _1, stapl::bind2nd(p_fun(), init1), init2));

  typedef ret_type (*sfun_t)(iterator_type, iterator_type, pred,
                             data_type const&);
  function<ret_type (iterator_type, iterator_type)>
    sfp(bind((sfun_t) std::replace_if<iterator_type, pred, data_type>,
             _1, _2, stapl::bind2nd(fun(), init1), init2));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(0, 1)))
    (tres, i, pfp, sfp);

  test_report(tres);
}

template<typename T>
void test_replace_copy_if(const unsigned int n, const unsigned int i,
                          traits_type const& t, char const* name,
                          std::string const& ds = "none")
{
  typedef void                          ret_type;
  typedef test_two_view<ret_type>       test_type;

  typedef stapl::less<data_type>        p_fun;
  typedef stapl::binder2nd<p_fun,data_type,false>      p_pred;

  typedef std::less<data_type>          fun;
  typedef stapl::binder2nd<fun,data_type,false>        pred;

  const data_type init1 = 10;
  const data_type init2 = 99;

  typedef view_type::iterator (*pfun_t)(view_type&, view_type&, p_pred,
                                        const data_type);

  function<ret_type (view_type&, view_type&)>
    pfp(bind((pfun_t) stapl::replace_copy_if<view_type, view_type, p_pred>,
             _1, _2, stapl::bind2nd(p_fun(), init1), init2));

  typedef iterator_type (*sfun_t)(iterator_type, iterator_type, iterator_type,
                                  pred, data_type const&);

  function<ret_type (iterator_type, iterator_type, iterator_type)>
    sfp(bind((sfun_t) std::replace_copy_if<iterator_type, iterator_type, pred,
                                     data_type>,
             _1, _2, _3, stapl::bind2nd(fun(),init1), init2));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(0, 1)),
    data_descriptor<pcontainer_type, view_type, null_sequence<data_type> >(
      n_elems, t.ct_create[1], t.vw_create[1], null_sequence<data_type>()))
    (tres, i, pfp, sfp);

  test_report(tres);
}

template<typename T>
void test_fill(const unsigned int n, const unsigned int i,
               traits_type const& t, char const* name,
               std::string const& ds = "none")
{
  typedef void                       ret_type;
  typedef test_one_view<ret_type>    test_type;

  const data_type init = 42;

  typedef ret_type (*pfun_t)(view_type const&, data_type);
  function<ret_type (view_type&)> pfp =
    bind((pfun_t)stapl::fill<view_type>, _1, init);

  typedef ret_type (*sfun_t)(iterator_type, iterator_type, data_type const&);
  function<ret_type (iterator_type, iterator_type)>
    sfp(bind((sfun_t)std::fill<iterator_type, data_type>, _1, _2, init));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, null_sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], null_sequence<data_type>()))
    (tres, i, pfp, sfp);

  test_report(tres);
}


// The wrapper is needed to supply the consistent interface with the two
// iterators to the test functions.
template<typename Iterator, typename Size, typename T>
void fill_n_wrapper(Iterator i1, Iterator, Size s, const T& v)
{ std::fill_n(i1, s, v); }


template<typename T>
void test_fill_n(const unsigned int n, const unsigned int i,
                 traits_type const& t, char const* name,
                 std::string const& ds = "none")
{
  typedef void                     ret_type;
  typedef test_one_view<ret_type>  test_type;

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  const data_type init = 42;
  const size_t size = n_elems * 0.98;

  typedef ret_type (*pfun_t)(view_type&, data_type, size_t);
  function<ret_type (view_type&)>
    pfp = bind((pfun_t)stapl::fill_n<view_type>, _1, init, size);

  typedef ret_type (*sfun_t)(iterator_type, iterator_type, size_t,
                             data_type const&);
  function<ret_type (iterator_type, iterator_type)>
    sfp(bind((sfun_t)fill_n_wrapper<iterator_type, size_t, data_type>,
             _1, _2, size, init));

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, null_sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], null_sequence<data_type>()))
    (tres, i, pfp, sfp);

  test_report(tres);
}


template<typename T>
void test_generate(const unsigned int n, const unsigned int i,
                   traits_type const& t, char const* name,
                   std::string const& ds = "none")
{
  typedef void                         ret_type;
  typedef test_one_view<ret_type>      test_type;

  typedef ret_type (*sfun_t)(iterator_type, iterator_type, sequence<data_type>);
  typedef ret_type (*pfun_t)(view_type const&, sequence<data_type>);

  function<ret_type (iterator_type, iterator_type)>
    sfp(bind((sfun_t) std::generate<iterator_type, sequence<data_type> >,
             _1, _2, sequence<data_type>(0, 1)));

  function<ret_type (view_type const&)>
    pfp = bind((pfun_t) stapl::generate<view_type, sequence<data_type> >,
               _1, sequence<data_type>(0, 1));

  const unsigned long int n_elems = set_data_size(n, ds, "mutating");

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, null_sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], null_sequence<data_type>()))
    (tres, i, pfp, sfp);

  test_report(tres);
}


// The wrapper is needed to supply the consistent interface with the
// two iterators to the test functions.
template<typename Iterator, typename Size, typename Generator>
void generate_n_wrapper(Iterator i1, Iterator, Size count, Generator g)
{ std::generate_n(i1, count, g); }


// This should be higher in the test dependency tree (if any)
// std::generate_n doesn't exist and this test is not the best one
// (see that it always fills the container)
template<typename T>
void test_generate_n(const unsigned int n, const unsigned int i,
                     traits_type const& t, char const* name,
                     std::string const& ds = "none")
{
  typedef void                                    ret_type;
  typedef test_one_view<ret_type>                 test_type;

  typedef ret_type (*sfun_t)(iterator_type, iterator_type, size_t,
                             sequence<data_type>);
  typedef ret_type (*pfun_t)(view_type const&, size_t, size_t,
                             sequence<data_type>);

  unsigned long int n_elems = set_data_size(n, ds, "mutating");

  //FIXME: Resolve memory consumtion issues.
  // Reduce the data size so the runtime is on the appropriate scale.
  if ((ds != "tiny") && (ds != "none"))
  {
    n_elems /= 1000;
  }

  const size_t size = n_elems/2;

  function<ret_type (view_type const&)>
    pfp(bind((pfun_t) stapl::generate_n<view_type, sequence<data_type> >,
             _1, 0, size, sequence<data_type>(0, 1)));

  function<ret_type (iterator_type, iterator_type)>
    sfp(bind((sfun_t) generate_n_wrapper<iterator_type, size_t,
                                         sequence<data_type> >,
             _1, _2, size, sequence<data_type>(0, 1)));

  timed_test_result tres(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, null_sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], null_sequence<data_type>()))
    (tres, i, pfp, sfp);

  test_report(tres);
}


stapl::exit_code stapl_main(int argc, char* argv[])
{
  typedef void (*test_func_type)(const unsigned int, const unsigned int,
                                 traits_type const&, char const*,
                                 std::string const&);

  test_pair<test_func_type> tests[] =
  {
    {"for_each",            test_for_each<int>},
    {"copy",                test_copy<int>},
    {"copy_n",              test_copy_n<int>},
    {"swap_ranges",         test_swap_ranges<int>},
    {"transform",           test_transform<int>},
    {"replace",             test_replace<int>},
    {"replace_copy",        test_replace_copy<int>},
    {"replace_if",          test_replace_if<int>},
    {"replace_copy_if",     test_replace_copy_if<int>},
    {"fill",                test_fill<int>},
    {"fill_n",              test_fill_n<int>},
    {"generate",            test_generate<int>},
    {"generate_n",          test_generate_n<int>},
  };

  test_execute(argc, argv, tests,
               tests+(sizeof (tests)/sizeof (test_pair<test_func_type>)));

  return EXIT_SUCCESS;
}
