/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#define REPORT_WITH_COLOR

#include <stapl/runtime.hpp>
#include <stapl/utility/tuple/ensure_tuple.hpp>

#include "../../test_report.hpp"

using namespace stapl;

void test_already_tuple()
{
  using tup = std::tuple<char, float>;
  using result = tuple_ops::ensure_tuple_t<tup>;

  static_assert(std::is_same<tup, result>::value, "ensure_tuple_t<tuple<char,float>>");
}

void test_single_value()
{
  using result = tuple_ops::ensure_tuple_t<char>;
  using expected = std::tuple<char>;

  static_assert(std::is_same<result, expected>::value, "ensure_tuple_t<char>");
}

void test_single_value_runtime()
{
  std::size_t x = 5ul;
  auto result = tuple_ops::ensure_tuple(x);

  STAPL_TEST_REPORT(get<0>(result) == x, "ensure_tuple(size_t)");
}

void test_already_tuple_runtime()
{
  auto tup = make_tuple(5.0, 'c');
  auto result = tuple_ops::ensure_tuple(tup);

  const bool passed =
    get<0>(result) == get<0>(tup) && get<1>(result) == get<1>(tup);

  STAPL_TEST_REPORT(passed, "ensure_tuple(tuple<double, char>)");
}

exit_code stapl_main(int argc, char** argv)
{
  test_single_value_runtime();
  test_already_tuple_runtime();

  return EXIT_SUCCESS;
}
