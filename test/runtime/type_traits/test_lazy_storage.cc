/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE lazy_storage
#include "utility.h"
#include <stapl/runtime/type_traits/lazy_storage.hpp>

template<typename T>
union wrapper
{
  stapl::lazy_storage<T> storage;
  int                    i;
};

struct empty
{ };

constexpr bool operator==(empty const&, empty const&) noexcept
{ return true; }

BOOST_AUTO_TEST_CASE( test_empty )
{
  typedef empty value_type;

  wrapper<value_type> u;

  u.storage.construct(value_type());
  value_type v = u.storage.moveout();

  BOOST_CHECK( v==value_type() );
}

BOOST_AUTO_TEST_CASE( test_int )
{
  typedef int value_type;

  wrapper<value_type> u;

  u.storage.construct(42);
  value_type v = u.storage.moveout();

  BOOST_CHECK_EQUAL( v, 42 );
}


class A
{
public:
  static int constructed;

  int i;

public:
  A(void)
  : i(42)
  { ++constructed; }

  A(A const& other)
  : i(other.i)
  { ++constructed; }

  ~A(void)
  { --constructed; }
};
int A::constructed = 0;

constexpr bool operator==(A const & a0, A const& a1)
{ return (a0.i==a1.i); }

BOOST_AUTO_TEST_CASE( test_A )
{
  typedef A value_type;

  BOOST_CHECK_EQUAL( A::constructed, 0 );
  {
    const value_type iv;
    BOOST_CHECK_EQUAL( A::constructed, 1 );

    wrapper<value_type> u;
    BOOST_CHECK_EQUAL( A::constructed, 1 );

    u.storage.construct(iv);
    BOOST_CHECK_EQUAL( A::constructed, 2 );

    value_type v = u.storage.moveout();
    BOOST_CHECK_EQUAL( A::constructed, 2 );

    BOOST_CHECK( v==iv );
  }
  BOOST_CHECK_EQUAL( A::constructed, 0 );
}
