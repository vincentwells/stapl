#!/bin/sh

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

TEST_OPTIONS='--report_level=no --log_level=nothing'
TEST_OUTPUT_PREFIX='(STAPL RTS type traits)'

eval ./test_aligned_storage $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_aligned_storage - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_aligned_storage - [passed]"
fi

eval ./test_has_define_type $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_has_define_type - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_has_define_type - [passed]"
fi

eval ./test_is_basic $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_is_basic - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_is_basic - [passed]"
fi

eval ./test_is_non_commutative $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_is_non_commutative - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_is_non_commutative - [passed]"
fi

eval ./test_is_p_object $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_is_p_object - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_is_p_object - [passed]"
fi

eval ./test_is_reference_wrapper $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_is_reference_wrapper - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_is_reference_wrapper - [passed]"
fi

eval ./test_is_shared_ptr $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_is_shared_ptr - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_is_shared_ptr - [passed]"
fi

eval ./test_lazy_storage $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_lazy_storage - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_lazy_storage - [passed]"
fi

eval ./test_supports_stapl_packing $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_supports_stapl_packing - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_supports_stapl_packing - [passed]"
fi

eval ./test_type_id $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_type_id - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_type_id - [passed]"
fi
