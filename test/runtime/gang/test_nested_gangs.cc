/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test nested gang creation of 1 location.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <stapl/runtime/utility/functional.hpp>
#include <iostream>
#include <numeric>
#include <cmath>
#include <vector>
#include "../test_utils.h"

struct simple_p_object
: public stapl::p_object
{ };

struct p_test
: public p_test_object
{
  // creates new gangs with just one location
  void test_empty(void)
  {
    stapl::gang g;
    STAPL_RUNTIME_TEST_CHECK(1, stapl::get_num_locations());
    STAPL_RUNTIME_TEST_CHECK(0, stapl::get_location_id());
    {
      stapl::gang g;
      STAPL_RUNTIME_TEST_CHECK(1, stapl::get_num_locations());
      STAPL_RUNTIME_TEST_CHECK(0, stapl::get_location_id());
    }
    STAPL_RUNTIME_TEST_CHECK(1, stapl::get_num_locations());
    STAPL_RUNTIME_TEST_CHECK(0, stapl::get_location_id());
  }

  // creates new gangs with just one location and one object
  void test_one(void)
  {
    stapl::gang g;
    simple_p_object o;
    STAPL_RUNTIME_TEST_CHECK(o.get_num_locations(),
                             stapl::get_num_locations());
    STAPL_RUNTIME_TEST_CHECK(o.get_location_id(),
                             stapl::get_location_id());
    {
      stapl::gang g;
      simple_p_object o;
      STAPL_RUNTIME_TEST_CHECK(o.get_num_locations(),
                               stapl::get_num_locations());
      STAPL_RUNTIME_TEST_CHECK(o.get_location_id(),
                               stapl::get_location_id());
    }
    STAPL_RUNTIME_TEST_CHECK(o.get_num_locations(),
                             stapl::get_num_locations());
    STAPL_RUNTIME_TEST_CHECK(o.get_location_id(),
                             stapl::get_location_id());
  }

  // creates new gangs with one heap-allocated object on the outer
  void test_outer(void)
  {
    simple_p_object *p = 0;
    {
      stapl::gang g;
      p = new simple_p_object;
      STAPL_RUNTIME_TEST_CHECK(p->get_num_locations(),
                               stapl::get_num_locations());
      STAPL_RUNTIME_TEST_CHECK(p->get_location_id(),
                               stapl::get_location_id());
      {
        stapl::gang g;
        STAPL_RUNTIME_TEST_CHECK(1, stapl::get_num_locations());
        STAPL_RUNTIME_TEST_CHECK(0, stapl::get_location_id());
      }
      STAPL_RUNTIME_TEST_CHECK(p->get_num_locations(),
                               stapl::get_num_locations());
      STAPL_RUNTIME_TEST_CHECK(p->get_location_id(),
                               stapl::get_location_id());
    }
    stapl::p_object_delete<simple_p_object> d;
    d(p);
    stapl::rmi_fence(); // wait for p_object_delete to finish
  }

  // creates new gangs with one heap-allocated object in the inner
  void test_inner(void)
  {
    simple_p_object *p = 0;
    {
      stapl::gang g;
      STAPL_RUNTIME_TEST_CHECK(1, stapl::get_num_locations());
      STAPL_RUNTIME_TEST_CHECK(0, stapl::get_location_id());
      {
        stapl::gang g;
        p = new simple_p_object;
        STAPL_RUNTIME_TEST_CHECK(p->get_num_locations(),
                                 stapl::get_num_locations());
        STAPL_RUNTIME_TEST_CHECK(p->get_location_id(),
                                 stapl::get_location_id());
      }
      STAPL_RUNTIME_TEST_CHECK(1, stapl::get_num_locations());
      STAPL_RUNTIME_TEST_CHECK(0, stapl::get_location_id());
    }
    stapl::p_object_delete<simple_p_object> d;
    d(p);
    stapl::rmi_fence(); // wait for p_object_delete to finish
  }

  void execute(void)
  {
    test_empty();
    test_one();
    test_outer();
    test_inner();
  }
};


stapl::exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();
#ifndef _TEST_QUIET
  std::cout << stapl::get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
