/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test @ref stapl::location_specific_storage.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <stapl/runtime/location_specific_storage.hpp>
#include <iostream>
#include <vector>
#include "../test_utils.h"

const int N = 42;
stapl::location_specific_storage<int>              v(N);
stapl::location_specific_storage<std::vector<int>> vec(2, 1);


struct p_test
: public stapl::p_object
{
  void test_local(void)
  {
    STAPL_RUNTIME_TEST_CHECK(v.get(), N);
    {
      stapl::gang lg;
      STAPL_RUNTIME_TEST_CHECK(v.get(), N);
      ++v.get();
      STAPL_RUNTIME_TEST_CHECK(v.get(), (N + 1));
      STAPL_RUNTIME_TEST_CHECK(vec.get().size(), 2);
      STAPL_RUNTIME_TEST_CHECK(vec.get().at(0), 1);
      STAPL_RUNTIME_TEST_CHECK(vec.get().at(1), 1);
      vec.get() = {5, 6, 7};
    }
    STAPL_RUNTIME_TEST_CHECK(v.get(), N);
    STAPL_RUNTIME_TEST_CHECK(vec.get().size(), 2);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(0), 1);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(1), 1);
  }

  void test_nested(void)
  {
    STAPL_RUNTIME_TEST_CHECK(v.get(), N);
    vec.get() = {5, 6, 7};
    {
      stapl::gang g;
      STAPL_RUNTIME_TEST_CHECK(v.get(), N);
      ++v.get();
      STAPL_RUNTIME_TEST_CHECK(v.get(), (N + 1));
      STAPL_RUNTIME_TEST_CHECK(vec.get().size(), 2);
      STAPL_RUNTIME_TEST_CHECK(vec.get().at(0), 1);
      STAPL_RUNTIME_TEST_CHECK(vec.get().at(1), 1);
    }
    STAPL_RUNTIME_TEST_CHECK(v.get(), N);
  }

  void test_destroy(void)
  {
    vec.destroy();
    STAPL_RUNTIME_TEST_CHECK(vec.get().size(), 2);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(0), 1);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(1), 1);

    vec.get().push_back(42);
    STAPL_RUNTIME_TEST_CHECK(vec.get().size(), 3);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(0), 1);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(1), 1);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(2), 42);

    vec.destroy();
    STAPL_RUNTIME_TEST_CHECK(vec.get().size(), 2);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(0), 1);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(1), 1);
  }

  void test_reset(void)
  {
    vec.destroy();
    STAPL_RUNTIME_TEST_CHECK(vec.get().size(), 2);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(0), 1);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(1), 1);

    vec.reset(std::vector<int>{11, 12, 13});
    STAPL_RUNTIME_TEST_CHECK(vec.get().size(), 3);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(0), 11);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(1), 12);
    STAPL_RUNTIME_TEST_CHECK(vec.get().at(2), 13);
  }

  void execute(void)
  {
    test_local();
    for (int i=0; i<10; ++i)
      test_nested();
    test_destroy();
    test_reset();
  }
};


stapl::exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();
#ifndef _TEST_QUIET
  std::cout << stapl::get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
