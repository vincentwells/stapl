/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test @ref stapl::opaque_rmi() to all locations.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <iostream>
#include <vector>
#include "test_utils.h"

using namespace stapl;

struct p_test
: public p_object
{
  p_test_object m_obj;

  void test_void(void)
  {
    typedef void data_type;
    // test regular iteration
    futures<data_type> h1 = opaque_rmi(all_locations, m_obj.get_rmi_handle(),
                                       &p_test_object::set_sync,
                                       m_obj.get_location_id());
    STAPL_RUNTIME_TEST_CHECK(m_obj.get_num_locations(), h1.size());
    for (futures<data_type>::size_type i = 0; i<h1.size(); ++i) {
      h1.get(i);
    }

    rmi_fence(); // wait for all opaque_rmi calls to finish

    for (unsigned int i = 0; i < m_obj.get_num_locations(); ++i) {
      STAPL_RUNTIME_TEST_CHECK(true, m_obj.get_sync(i));
    }

    rmi_fence(); // quiescence before next test

    // test vector return
    futures<data_type> h2 = opaque_rmi(all_locations, m_obj.get_rmi_handle(),
                                       &p_test_object::set_sync,
                                       m_obj.get_location_id());
    STAPL_RUNTIME_TEST_CHECK(m_obj.get_num_locations(), h2.size());
    h2.get();

    rmi_fence(); // wait for all opaque_rmi calls to finish

    for (unsigned int i = 0; i < m_obj.get_num_locations(); ++i) {
      STAPL_RUNTIME_TEST_CHECK(true, m_obj.get_sync(i));
    }

    rmi_fence(); // quiescence before next test
  }

  void test_int(void)
  {
    typedef p_object::size_type data_type;
    // test regular iteration
    futures<data_type> h1 = opaque_rmi(all_locations, m_obj.get_rmi_handle(),
                                       &p_test_object::get_location_id);
    STAPL_RUNTIME_TEST_CHECK(m_obj.get_num_locations(), h1.size());
    for (futures<data_type>::size_type i = 0; i<h1.size(); ++i) {
      STAPL_RUNTIME_TEST_CHECK(i, h1.get(i));
    }

    rmi_fence(); // quiescence before next test

    // test vector return
    futures<data_type> h2 = opaque_rmi(all_locations, m_obj.get_rmi_handle(),
                                       &p_test_object::get_arg<data_type>,
                                       m_obj.get_location_id());
    STAPL_RUNTIME_TEST_CHECK(m_obj.get_num_locations(), h2.size());
    std::vector<data_type> v2 = h2.get();
    STAPL_RUNTIME_TEST_CHECK(m_obj.get_num_locations(), v2.size());
    for (std::vector<data_type>::size_type i = 0; i<v2.size(); ++i) {
      STAPL_RUNTIME_TEST_CHECK(m_obj.get_location_id(), v2[i]);
    }

    rmi_fence(); // quiescence before next test
  }

  void test_vector(void)
  {
    typedef std::vector<std::vector<double> >::size_type int_type;

    // test regular iteration
    futures<std::vector<double> > h1 =
      opaque_rmi(all_locations, m_obj.get_rmi_handle(),
                 &p_test_object::get_vector<double>);
    STAPL_RUNTIME_TEST_CHECK(this->get_num_locations(), h1.size());
    for (int_type i = 0; i<h1.size(); ++i) {
      std::vector<double> v = h1.get(i);
      STAPL_RUNTIME_TEST_CHECK(i, v.size());
      for (std::vector<double>::const_iterator it=v.begin(); it!=v.end(); ++it)
        STAPL_RUNTIME_TEST_CHECK(i, int_type(*it));
    }

    rmi_fence(); // quiescence before next test

    // test vector return
    futures<std::vector<double> > h2 =
      opaque_rmi(all_locations, m_obj.get_rmi_handle(),
                 &p_test_object::get_vector<double>);
    STAPL_RUNTIME_TEST_CHECK(this->get_num_locations(), h2.size());
    std::vector<std::vector<double> > v2 = h2.get();
    STAPL_RUNTIME_TEST_CHECK(this->get_num_locations(), v2.size());
    for (int_type i = 0; i<v2.size(); ++i) {
      std::vector<double>& v = v2[i];
      STAPL_RUNTIME_TEST_CHECK(i, v.size());
      for (std::vector<double>::const_iterator it=v.begin(); it!=v.end(); ++it)
        STAPL_RUNTIME_TEST_CHECK(i, int_type(*it));
    }

    rmi_fence(); // quiescence before next test
  }

  void execute(void)
  {
    test_void();
    test_int();
    test_vector();
  }
};


exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();
#ifndef _TEST_QUIET
  std::cout << get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
