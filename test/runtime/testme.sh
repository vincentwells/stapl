#!/bin/sh

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

run_command=$1
TEST_OUTPUT_PREFIX='(STAPL RTS)'

#
# core support
#

eval $run_command ./test_affinity
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_affinity - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_affinity - [passed]"
fi

eval $run_command ./test_alignment
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_alignment - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_alignment - [passed]"
fi

eval $run_command ./test_rmi_handle
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_rmi_handle - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_rmi_handle - [passed]"
fi


#
# point-to-point
#

eval $run_command ./test_async_rmi
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_async_rmi - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_async_rmi - [passed]"
fi

eval $run_command ./test_opaque_rmi
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_opaque_rmi - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_opaque_rmi - [passed]"
fi

eval $run_command ./test_try_rmi
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_try_rmi - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_try_rmi - [passed]"
fi

eval $run_command ./test_sync_rmi
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_sync_rmi - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_sync_rmi - [passed]"
fi


#
# asynchronous result
#

eval $run_command ./test_promise
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_promise - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_promise - [passed]"
fi

eval $run_command ./test_future
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_future - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_future - [passed]"
fi


#
# synchronization
#

eval $run_command ./test_rmi_barrier
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_rmi_barrier - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_rmi_barrier - [passed]"
fi

eval $run_command ./test_rmi_fence
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_rmi_fence - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_rmi_fence - [passed]"
fi

eval $run_command ./test_rmi_synchronize
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_rmi_synchronize - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_rmi_synchronize - [passed]"
fi


#
# collective
#

eval $run_command ./test_allgather_rmi
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_allgather_rmi - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_allgather_rmi - [passed]"
fi

eval $run_command ./test_allreduce_rmi
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_allreduce_rmi - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_allreduce_rmi - [passed]"
fi

eval $run_command ./test_allreduce_object
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_allreduce_object - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_allreduce_object - [passed]"
fi

eval $run_command ./test_broadcast_rmi
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_broadcast_rmi - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_broadcast_rmi - [passed]"
fi


#
# one-sided
#

eval $run_command ./test_async_rmi_all
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_async_rmi_all - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_async_rmi_all - [passed]"
fi

eval $run_command ./test_async_rmi_range
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_async_rmi_range - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_async_rmi_range - [passed]"
fi

eval $run_command ./test_opaque_rmi_all
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_opaque_rmi_all - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_opaque_rmi_all - [passed]"
fi

eval $run_command ./test_reduce_rmi
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_reduce_rmi - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_reduce_rmi - [passed]"
fi


#
# communication patterns
#

eval $run_command ./test_async_patterns
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_async_patterns - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_async_patterns - [passed]"
fi

eval $run_command ./test_comm_patterns
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_comm_patterns - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_comm_patterns - [passed]"
fi

eval $run_command ./test_list_ranking
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_list_ranking - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_list_ranking - [passed]"
fi


#
# special objects
#

eval $run_command ./test_args
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_args - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_args - [passed]"
fi

eval $run_command ./test_big_arguments
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_big_arguments - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_big_arguments - [passed]"
fi

eval $run_command ./test_bind
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_bind - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_bind - [passed]"
fi

eval $run_command ./test_p_object
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_p_object - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_p_object - [passed]"
fi


#
# other
#

eval $run_command ./test_construct
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_construct - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_construct - [passed]"
fi

eval $run_command ./test_async_construct
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_async_construct - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_async_construct - [passed]"
fi

eval $run_command ./test_bind_rmi
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_bind_rmi - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_bind_rmi - [passed]"
fi

# SEE gforge TODO 1371
#eval $run_command ./test_restore
#if test $? != 0
#then
#  echo $TEST_OUTPUT_PREFIX" test_restore - [FAILED]"
#else
#  echo $TEST_OUTPUT_PREFIX" test_restore - [passed]"
#fi

eval $run_command ./test_immutable_ref
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_immutable_ref - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_immutable_ref - [passed]"
fi

eval $run_command ./test_immutable_shared
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_immutable_shared - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_immutable_shared - [passed]"
fi

eval $run_command ./test_orphan_location
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_orphan_location - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_orphan_location - [passed]"
fi

eval $run_command ./test_range
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_range - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_range - [passed]"
fi

eval $run_command ./test_immutable_range
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_immutable_range - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_immutable_range - [passed]"
fi

eval $run_command ./test_zero_copy
if test $? != 0
then
  echo $TEST_OUTPUT_PREFIX" test_zero_copy - [FAILED]"
else
  echo $TEST_OUTPUT_PREFIX" test_zero_copy - [passed]"
fi
