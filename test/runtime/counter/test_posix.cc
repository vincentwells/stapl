/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE posix_timers
#include "utility.h"
#include <chrono>
#include <thread>
#include <stapl/runtime/counter/counter.hpp>
#include <stapl/runtime/counter/posix/clock_gettime_timer.hpp>
#include <stapl/runtime/counter/posix/getrusage_timer.hpp>
#include <stapl/runtime/counter/posix/gettimeofday_timer.hpp>
#include <cmath>

using namespace stapl;

static void loop(void)
{
  std::this_thread::sleep_for(std::chrono::seconds{1});
}

BOOST_AUTO_TEST_CASE( test_clock_gettime_timer )
{
  counter<clock_gettime_timer> c;
  std::cout << c.native_name() << ": " << c.read() << ' ';

  c.start();
  loop();
  std::cout << c.stop() << std::endl;
}

BOOST_AUTO_TEST_CASE( test_getrusage_timer )
{
  counter<getrusage_timer> c;
  std::cout << c.native_name() << ": " << std::endl;

  c.start();
  loop();
  counter<getrusage_timer>::value_type v = c.stop();
  std::cout << "\tru_utime.tv_sec:  " << v.ru_utime.tv_sec << std::endl;
  std::cout << "\tru_utime.tv_usec: " << v.ru_utime.tv_usec << std::endl;
  std::cout << "\tru_stime.tv_sec:  " << v.ru_stime.tv_sec << std::endl;
  std::cout << "\tru_stime.tv_usec: " << v.ru_stime.tv_usec << std::endl;
  std::cout << "\tru_maxrss:        " << v.ru_maxrss << std::endl;
  std::cout << "\tru_ixrss:         " << v.ru_ixrss << std::endl;
  std::cout << "\tru_idrss:         " << v.ru_idrss << std::endl;
  std::cout << "\tru_isrss:         " << v.ru_isrss << std::endl;
  std::cout << "\tru_minflt:        " << v.ru_minflt << std::endl;
  std::cout << "\tru_majflt:        " << v.ru_majflt << std::endl;
  std::cout << "\tru_nswap:         " << v.ru_nswap << std::endl;
  std::cout << "\tru_inblock:       " << v.ru_inblock << std::endl;
  std::cout << "\tru_oublock:       " << v.ru_oublock << std::endl;
  std::cout << "\tru_msgsnd:        " << v.ru_msgsnd << std::endl;
  std::cout << "\tru_msgrcv:        " << v.ru_msgrcv << std::endl;
  std::cout << "\tru_nsignals       " << v.ru_nsignals << std::endl;
  std::cout << "\tru_nvcsw:         " << v.ru_nvcsw << std::endl;
  std::cout << "\tru_nivcsw:        " << v.ru_nivcsw << std::endl;
}

BOOST_AUTO_TEST_CASE( test_gettimeofday_timer )
{
  counter<gettimeofday_timer> c;
  std::cout << c.native_name() << ": " << c.read() << ' ';

  c.start();
  loop();
  std::cout << c.stop() << std::endl;
}
