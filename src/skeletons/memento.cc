/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/skeletons/executors/memento.hpp>

namespace stapl {
namespace skeletons {

memento::memento()
  : callbacks(new internal_stack_t())
{ }

//////////////////////////////////////////////////////////////////////
/// @brief the front element of the memento stack at times needs to
/// stays untouched until some criteria is met. The entities that can
/// remain untouched are called lazy. This method checks if the
/// element is lazy.
///
/// @return true only if the element on top of the memento stack is
///              lazy
//////////////////////////////////////////////////////////////////////
bool memento::front_is_lazy()
{
  return callbacks->front().is_lazy();
}

std::size_t memento::size() const
{
  return callbacks->size();
}

bool memento::is_empty()
{
  return callbacks->empty();
}

void memento::pop()
{
  stapl_assert(callbacks->size() > 0,
               "Pop is called on an empty memento stack");
  callbacks->pop_front_and_dispose(
    skeletons_impl::memento_element_disposer());
}

//////////////////////////////////////////////////////////////////////
/// @brief This method resumes the spawning process of the front
/// element of the memento dequeue as long as there is nothing else to
/// do and the front element is not lazy. If all the elements of the
/// memento double-ended queue are already resumed and there is nothing
/// else left to do nothing will be done.
///
/// The skeleton manager which holds this memento stack will finish
/// the spawning process if there is nothing left to spawn in this
/// memento stack.
///
/// @param ignore_lazyness if true, continue the spawning process even
///                        if the front element of the memento is lazy
//////////////////////////////////////////////////////////////////////
void memento::resume(bool ignore_lazyness)
{
  if ((ignore_lazyness || !this->front_is_lazy()) &&
      !is_empty()) {
    auto&& f = callbacks->front(); //store f as f() may modify the memento
    this->pop_keep();
    f();
    skeletons_impl::memento_element_disposer()(&f);
  }
}

void memento::pop_keep()
{
  stapl_assert(callbacks->size() > 0,
               "Pop is called on an empty memento stack");
  callbacks->pop_front();
}

void memento::push_back(element_type* element, bool is_lazy)
{
  element->set_is_lazy(is_lazy);
  callbacks->push_back(*element);
}

void memento::push_front(element_type* element, bool is_lazy)
{
  element->set_is_lazy(is_lazy);
  callbacks->push_front(*element);
}

} // namespace skeletons
} // namespace stapl