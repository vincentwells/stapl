/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <cmath>
#include <vector>
#include <stapl/skeletons/explicit/tree_info.h>

namespace stapl {

namespace paragraph_impl {

int compute_combine_trees(float const& n, std::vector<tree_info>& trees)
{
  int elem_remaining = int(n);
  int leaf_offset    = 0;
  int id_offset      = 0;

  for (int curr_width = int(std::pow(2.0,std::floor(std::log2(n))));
       curr_width != 0 && elem_remaining != 0;
       curr_width >>= 1)
  {
    if (curr_width <= elem_remaining)
    {
      tree_info t;

      t.width = curr_width;
      t.height = int(std::log2(curr_width));
      t.leaf_offset = leaf_offset;
      t.id_offset = id_offset;
      t.root_task_id = id_offset + 2*curr_width - 2;
      t.curr_id = id_offset + curr_width; //curr_id is not used for map tasks
      t.elements_remaining = 0; // elements_remaining is not used for map_reduce

      trees.push_back(t);

      elem_remaining -= curr_width;
      leaf_offset += curr_width;
      id_offset += 2*curr_width - 1;
    }
  }
  return int(std::ceil(std::log2(n)));
}

} // namespace paragraph_impl

} // namespace stapl
