/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <utility>
#include <iostream>
#include <stapl/utility/do_once.hpp>
#include <stapl/domains/continuous.hpp>

stapl::exit_code stapl_main(int argc, char **argv)
{

  // Creating an infinite string domain, two initialized continuous
  // double domains and one uninitialized continuous domain.
  stapl::continuous_domain<std::string> infinite_string("a", "z");
  stapl::continuous_domain<double> continuous_double(0.0, 1.0);
  stapl::continuous_domain<double> continuous_double_2(0.25, 1.75);
  stapl::continuous_domain<double> intersection;

  // Intersection of continuous_double and continuous_double_2 domains.
  // Note the "&" operator for intersection of domains.
  intersection = continuous_double & continuous_double_2;

  // Print domains information. The use of "&" in the lambda function
  // captures all variables used in the lambda body by reference.
  // With this, it is not necessary to add the variables in stapl::do_once.
  stapl::do_once(
    [&]{

      // Print the domain. Note the overloaded operator "<<" for
      // domains.
      std::cout << "infinite_string domain: " << infinite_string
                << std::endl;
      std::cout << "continuous_double domain: " << continuous_double
                << std::endl;

      // Print sizes of infinite_string and continuous_double domains.
      std::cout << "size of infinite_string domain: "
                << infinite_string.size() << std::endl;
      std::cout << "size of continuous_double domain: "
                << continuous_double.size() << std::endl;

      // Print some examples of domain elements.
      std::cout << "infinite_string contains \"ab\"? "
                << infinite_string.contains("ab") << std::endl;
      std::cout << "infinite_string contains \"any word\"? "
                << infinite_string.contains("any word") << std::endl;
      std::cout << "infinite_string contains "
                << "\"Honorificabilitudinitatibus\"? "
                << infinite_string.contains("Honorificabilitudinitatibus")
                << std::endl;
      std::cout << "infinite_string contains \"@#?%!\"? "
                << infinite_string.contains("@#?%!") << std::endl;
      std::cout << "continuous_double contains 0.00324? "
                << continuous_double.contains(0.00324) << std::endl;
      std::cout << "continuous_double contains 1.00324? "
                << continuous_double.contains(1.00324) << std::endl;
      std::cout << "intersection of " << continuous_double << " and "
                << continuous_double_2 << " is: " << intersection
                << std::endl;
    }
  );

  return EXIT_SUCCESS;
}
