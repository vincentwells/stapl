/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/array.hpp>
#include <stapl/algorithm.hpp>
#include <stapl/utility/do_once.hpp>

struct prime
{
  typedef bool result_type;

  result_type operator()(int i)
  {
    int c = 0;
    for (int a = 1; a <= i; ++a)
    {
      if(i%a==0) c++;
    }

    if (c==2)
    {
      return false;
    }else
    {
      return true;
    }
  }
};

stapl::exit_code stapl_main(int argc, char* argv[])
{

  typedef size_t value_t;
  typedef stapl::array_view<stapl::array<value_t>> view_type;
  size_t n = atol(argv[1]);
  stapl::array<size_t> c(n);
  stapl::array_view<stapl::array<size_t>> v(c);

  //Fill the array with sequential values, from 1 to n.
  stapl::iota(v,1);

  //Replace all array values that are not prime numbers with the value 0.
  stapl::replace_if(v, prime(), 0);

  //Compute the sum of all the values in the array.
  size_t s = stapl::accumulate(v, 0);

  stapl::do_once([&]{
    std::cout << s << std::endl;
  });

  return EXIT_SUCCESS;

}
