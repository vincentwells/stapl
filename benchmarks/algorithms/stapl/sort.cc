/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include "../utilities.hpp"

#include <vector>
#include <iostream>
#include <stapl/array.hpp>

stapl::exit_code stapl_main(int argc, char *argv[])
{
  size_t size = atoll(argv[1]);
  int weak_scaling = 0;
  int data_distribution = 0; // 0=balanced, 1=block, 2=block_cyclic
  stapl::distribution_spec<> data_dist = stapl::balance(size);
  int block_size=2048;

  if (argc > 4)
  {
    weak_scaling = atoi(argv[2]);
    data_distribution = atoi(argv[3]);
    block_size = atoi(argv[4]);
  }

  if (data_distribution == 1)
  {
    data_dist = stapl::block(size,block_size);
  }
  else if (data_distribution == 2)
  {
    data_dist = stapl::block_cyclic(size,block_size);
  }

  std::vector<double> min_samples(10,0.), max_samples(10,0.),
    lex_samples(10,0.);

  bool min_correct(true), max_correct(true), lex_correct(true);

  counter_t timer;

  // MIN ELEMENT
  for (int sample = 0; sample != 10; ++sample)
  {
    array_type xcont(data_dist);
    array_type_vw x(xcont);

    fill_random(x, weak_scaling);
    timer.reset();
    timer.start();

    auto result = stapl::min_element(x);

    min_samples[sample] = timer.stop();

    // Validate
    auto result0 = stapl::is_null_reference(
      stapl::find_if(x, std::bind(std::less<double>(),
      std::placeholders::_1, result)));

    min_correct = min_correct && result0;
  }
  report_result("b_min_element","STAPL", min_correct, min_samples);

  // MAX ELEMENT
  for (int sample = 0; sample != 10; ++sample)
  {
    array_type xcont(data_dist);
    array_type_vw x(xcont);

    fill_random(x, weak_scaling);
    timer.reset();
    timer.start();

    auto result = stapl::max_element(x);

    max_samples[sample] = timer.stop();

    // Validate
    auto result0 = stapl::is_null_reference(
      stapl::find_if(x, std::bind(std::greater<double>(),
      std::placeholders::_1, result)));

    max_correct = max_correct && result0;
  }

  report_result("b_max_element","STAPL", max_correct, max_samples);

  // LEXICOGRAPHICAL COMPARE
  for (int sample = 0; sample != 10; ++sample)
  {
    array_type xcont(data_dist);
    array_type_vw x(xcont);

    fill_random(x, weak_scaling);
    array_type ycont(xcont);
    array_type_vw y(ycont);
    timer.reset();
    timer.start();

    bool first_less = stapl::lexicographical_compare(x, y);

    lex_samples[sample] = timer.stop();

    lex_correct = lex_correct && !first_less;
  }
  report_result("b_lex_compare","STAPL", lex_correct, lex_samples);

  return EXIT_SUCCESS;
}
