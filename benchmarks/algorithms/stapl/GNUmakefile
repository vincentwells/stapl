# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ifndef STAPL
   export STAPL = $(shell echo "$(PWD)" | sed 's,/benchmarks/algorithms/stapl,,')
endif

include $(STAPL)/GNUmakefile.STAPLdefaults

default: compile

all: compile

test: all
	$(MAKE) -l 0.0 runtests

runtests:
	$(call staplrun,1) ./basic
	$(call staplrun,1) ./numeric 262144
	$(call staplrun,1) ./non_mutating 262144
	$(call staplrun,1) ./sort 262144
	$(call staplrun,1) ./sort_define_dag 262144
	$(call staplrun,4) ./numeric 262144
	$(call staplrun,4) ./non_mutating 262144
	$(call staplrun,4) ./sort 262144
	$(call staplrun,4) ./sort_define_dag 262144

compile: numeric non_mutating mutating sort basic sort_define_dag

sort_define_dag: CXXFLAGS+=-DUSE_NEW_NOTATION
sort_define_dag: sort.cc
	${CC} ${STAPL_CXXFLAGS} ${CXXFLAGS} -o $@ $< ${STAPL_LIBRARIES} ${LIB} ${LIB_EPILOGUE}

clean:
	rm -f basic numeric non_mutating mutating sort sort_define_dag *TVD*

