/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_BENCHMARKS_FMM_KERNEL_H
#define STAPL_BENCHMARKS_FMM_KERNEL_H

#include <cmath>
#include "types.h"

namespace kernel {

/// P2P kernel between cells Ci and Cj
void P2P(C_iter Ci, C_iter Cj, real_t eps2, vec3 Xperiodic, bool mutual);
/// P2P kernel for cell C
void P2P(C_iter C, real_t eps2);
/// P2M kernel for cell C
void P2M(C_iter C);
/// M2M kernel for one parent cell Ci
void M2M(C_iter Ci, C_iter C0);
/// M2L kernel between cells Ci and Cj
void M2L(C_iter Ci, C_iter Cj, vec3 Xperiodic, bool mutual);
/// L2L kernel for one child cell Ci
void L2L(C_iter Ci, C_iter C0);
/// L2P kernel for cell Ci
void L2P(C_iter Ci);

};

#include "../kernels/LaplaceCartesianCPU.cc"
#include "../kernels/LaplaceP2PCPU.cc"

#endif // STAPL_BENCHMARKS_FMM_KERNEL_H
