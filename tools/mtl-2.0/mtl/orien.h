/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef MTL_ORIEN_H
#define MTL_ORIEN_H

namespace mtl {

struct row_orien;
struct column_orien;
struct row_major;
struct column_major;

//: Row Orientation
// This is used in matrix_implementation and in the indexer objects
// to map (row,column) to (major,minor).
struct row_orien {
  template <int MM, int NN> struct dims { enum { M = MM, N = NN }; };
  typedef row_tag orientation;
  typedef row_major constructor_tag;
  typedef column_orien transpose_type;
  template <class Dim>
  static typename Dim::size_type row(Dim d) { return d.first(); }
  template <class Dim>
  static typename Dim::size_type column(Dim d){ return d.second();}
  template <class Dim>
  static Dim map(Dim d) { return d; }
};

//: Column Orientation
// This is used in matrix_implementation and in the indexer objects
// to map (row,column) to (minor,major).
struct column_orien {
  template <int MM, int NN> struct dims { enum { M = NN, N = MM }; };
  typedef column_tag orientation;
  typedef row_orien transpose_type;
  typedef column_major constructor_tag;
  template <class Dim>
  static typename Dim::size_type row(Dim d) { return d.second(); }
  template <class Dim>
  static typename Dim::size_type column(Dim d){ return d.first(); }
#if 0
  /* the static dim has already been transposed in matrix.h
   so no need to do it here */
  template <class Dim>
  static typename Dim::transpose_type map(Dim d) { return d.transpose(); }
#else
  template <class Dim>
  static Dim map(Dim d) { return Dim(d.second(), d.first()); }
#endif
};

} /* namespace mtl */

#endif /* MTL_ORIEN_H */
