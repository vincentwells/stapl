/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef MTL_DIMENSION_H
#define MTL_DIMENSION_H

#include <utility>

namespace mtl {

  //: The Dimension Class
  //
  // This is similar to the std::pair class except that it can have static
  // parameters, and only deals with size types. The purpose of this
  // class is to transparently hide whether the dimensions of a matrix
  // are specified statically or dynamically.
  //
  //!category: utilities
  //!component: type
  //
template <class sizet, int MM = 0, int NN = 0>
class dimension {
	typedef dimension<sizet, MM, NN> self;
public:
  typedef dimension<sizet, NN, MM> transpose_type;
  typedef sizet size_type;
  enum { M = MM, N = NN };
  inline dimension() : m(0), n(0) { }
  inline dimension(const self& x) : m(x.m), n(x.n) { }
  template <class ST>
  inline dimension(const std::pair<ST,ST>& x) : m(x.first), n(x.second) { }
#if !defined( _MSVCPP_ )
	//template <class Dim>
	//inline dimension(const Dim& x) : m(x.m), n(x.n) { }
  template <class S, int MMM, int NNN>
  inline dimension(const dimension<S, MMM, NNN>& x) : m(x.m), n(x.n) {}
#endif
  inline dimension(size_type m_, size_type n_) : m(m_), n(n_) { }
  inline dimension& operator=(const dimension& x) {
    m = x.m; n = x.n; return *this; }
  inline size_type first() const { return M ? M : m; }
  inline size_type second() const { return N ? N : n; }
  inline bool is_static() const { return M != 0; }
  inline transpose_type transpose() const { return transpose_type(n, m); }
  /* protected: */
  size_type m, n;
};

} /* namespace mtl */

#endif /* MTL_DIMENSION_H */
