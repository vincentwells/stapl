/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PROXY_SET_H
#define STAPL_PROXY_SET_H

#include <set>

#include <stapl/views/proxy/proxy.hpp>
#include <stapl/views/iterator/member_iterator.h>
#include <stapl/views/proxy_macros.hpp>

namespace stapl {

STAPL_PROXY_HEADER_TEMPLATE(std::set, T)
{
  STAPL_PROXY_DEFINES(std::set<T>)
  STAPL_PROXY_REFLECT_TYPE(size_type)
  STAPL_PROXY_METHOD_RETURN(size, size_type)
  STAPL_PROXY_METHOD_RETURN(max_size, size_type)
  STAPL_PROXY_METHOD_RETURN(empty, bool)

  typedef typename target_t::const_iterator            const_iter_t;
  typedef typename target_t::const_reverse_iterator    const_r_iter_t;

  typedef member_iterator<const_iter_t, Accessor>      const_iterator;
  typedef member_iterator<const_r_iter_t, Accessor>    const_r_iterator;

  typedef T                                            value_type;

  const_iterator begin() const
  {
    return const_iterator(Accessor::const_invoke(&target_t::begin), *this);
  }

  const_iterator end() const
  {
    return const_iterator(Accessor::const_invoke(&target_t::end), *this);
  }

  const_r_iterator rbegin() const
  {
    return const_r_iterator(Accessor::const_invoke(&target_t::rbegin), *this);
  }

  const_r_iterator rend() const
  {
    return const_r_iterator(Accessor::const_invoke(&target_t::rend), *this);
  }

  const_iterator cbegin() const
  {
    return const_iterator(Accessor::const_invoke(&target_t::cbegin), *this);
  }

  const_iterator cend() const
  {
    return const_iterator(Accessor::const_invoke(&target_t::cend), *this);
  }

  const_r_iterator crbegin() const
  {
    return const_r_iterator(Accessor::const_invoke(&target_t::crbegin), *this);
  }

  const_r_iterator crend() const
  {
    return const_r_iterator(Accessor::const_invoke(&target_t::crend), *this);
  }

}; // struct proxy

} // namespace stapl

#endif
