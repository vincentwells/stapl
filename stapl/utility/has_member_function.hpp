/*
 // Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
 // component of the Texas A&M University System.

 // All rights reserved.

 // Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STAPL_UTILITY_HAS_MEMBER_FUNCTION_HPP
#define STAPL_UTILITY_HAS_MEMBER_FUNCTION_HPP

#include <boost/mpl/bool.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// \brief Macro which creates a type metafunction that deduces
/// whether a given struct/class defines a member function with a specified
/// name and signature.
///
/// @param func Name of member function to detect.
/// @param name Name of created metafunction.
///
/// @tparam T    Class type to inspect for existence of @p func.
/// @tparam Sign The type signature of @p func.
///
/// @ingroup utility

/// @note This approach does not detect the member function if it is defined in
/// a base class of T.
///
/// \note This uses esoteric functionality of the standard wrt to SFINAE. The
/// approach is taken from: http://stackoverflow.com/questions/257288/
////////////////////////////////////////////////////////////////////////
#define STAPL_HAS_MEMBER_FUNCTION(func, name)                       \
  template<typename T, typename Signature>                          \
  struct name                                                       \
  {                                                                 \
    typedef char yes[1];                                            \
    typedef char no[2];                                             \
                                                                    \
    template <typename U, U>                                        \
    struct type_check;                                              \
                                                                    \
    template <typename _1>                                          \
    static yes &chk(type_check<Signature, &_1::func> *);            \
                                                                    \
    template <typename   >                                          \
    static no &chk(...);                                            \
                                                                    \
    static bool const value = sizeof(chk<T>(0)) == sizeof(yes);     \
                                                                    \
    typedef boost::mpl::bool_<value> type;                          \
  }
} // namespace stapl

#endif // STAPL_UTILITY_HAS_MEMBER_FUNCTION_HPP
