/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_REVERSE_HPP
#define STAPL_UTILITY_TUPLE_REVERSE_HPP

#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/integer_sequence.hpp>
#include <stapl/utility/tuple/tuple_size.hpp>
#include <stapl/utility/tuple/tuple_element.hpp>
#include <stapl/utility/integer_sequence.hpp>

namespace stapl {

namespace result_of {

//////////////////////////////////////////////////////////////////////
/// @brief Reverses a given tuple. For example, tuple<char, int, double>
/// would become tuple<double, char, int>.
///
/// @tparam Tuple   a tuple to be reversed
/// @tparam IdxList an @c index_sequence to traverse the tuple
//////////////////////////////////////////////////////////////////////
template<
  typename Tuple,
  typename IdxList = make_index_sequence<tuple_size<Tuple>::value>>
struct reverse;


template<typename ...Elements, std::size_t... Indices>
struct reverse<tuple<Elements...>, index_sequence<Indices...>>
{
private:
  static constexpr size_t size = sizeof...(Indices);

public:
  using type = tuple<
                 typename tuple_element<
                   size - Indices - 1,
                   tuple<Elements...>>::type...>;
};


template<>
struct reverse<tuple<>, index_sequence<>>
{
  typedef tuple<> type;
};

} // namespace result_of

} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_REVERSE_HPP
