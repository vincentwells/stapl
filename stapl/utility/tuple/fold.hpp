/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_FOLD_HPP
#define STAPL_UTILITY_TUPLE_FOLD_HPP

#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/utility.hpp>

namespace stapl {
namespace tuple_ops {

namespace detail {

template<typename BinaryOp, typename T, int N, typename ...Elements>
struct fold_impl
{
  auto operator()(tuple<Elements...> const& elements,
                  T const& init,
                  BinaryOp const& binop) const
  STAPL_AUTO_RETURN(
    binop(
      fold_impl<BinaryOp, T, N-1, Elements...>()(elements, init, binop),
      get<N-1>(elements)
    )
  )
};


template<typename BinaryOp, typename T, typename ...Elements>
struct fold_impl<BinaryOp, T, 0, Elements...>
{
  T operator()(tuple<Elements...> const&,
               T const& init, BinaryOp const&) const
  {
    return init;
  }
};

} // namespace detail

//////////////////////////////////////////////////////////////////////
/// @brief Returns the result of the repeated application of binary
/// operation @c binop to the result of the previous @c binop invocation,
/// (or @c init if it is the first call) and each element of the tuple.
///
/// @param elements tuple to be folded
/// @param init     initial value
/// @param binop    binary operation
//////////////////////////////////////////////////////////////////////
template<typename BinaryOp, typename T, typename ...Elements>
inline auto
fold(tuple<Elements...> const& elements,
     T const& init, BinaryOp const& binop)
STAPL_AUTO_RETURN((
  detail::fold_impl<
    BinaryOp, T, sizeof...(Elements), Elements...
  >()(elements, init, binop)
))

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_FOLD_HPP
