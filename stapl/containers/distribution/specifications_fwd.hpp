/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_DISTRIBUTION_SPECIFICATIONS_FWD_HPP
#define STAPL_CONTAINERS_DISTRIBUTION_SPECIFICATIONS_FWD_HPP

#include <stapl/views/system_view.hpp>
#include <stapl/views/distribution_spec_view.hpp>
#include <stapl/utility/integer_sequence.hpp>
#include <stapl/domains/deferred.hpp>
#include "specification_functors.hpp"

namespace stapl {

namespace dist_spec_impl {

template <typename GIDDomain = stapl::indexed_domain<unsigned long int>>
struct distribution_spec
{
  typedef distribution_spec_view<
            distribution_spec_view<
              dist_view_impl::system_container,
              stapl::indexed_domain<location_type>,
              location_type, location_type
            >,
            deferred_domain<indexed_domain<unsigned long int>>,
            typename GIDDomain::index_type, unsigned long int
          >                                              mapping_view_type;
  typedef distribution_spec_view<
            mapping_view_type,
            GIDDomain,
            typename GIDDomain::index_type, unsigned long int
          >                                              partitioning_view_type;

  typedef partitioning_view_type type;
};

} // namespace dist_spec_impl


//////////////////////////////////////////////////////////////////////
/// @brief The type of the view that provides the specification
/// of a one-dimensional container distribution.
//////////////////////////////////////////////////////////////////////
template<typename GIDDomain = stapl::indexed_domain<unsigned long int>>
using distribution_spec =
  typename dist_spec_impl::distribution_spec<GIDDomain>::type;


namespace dist_spec_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Function invoked by view-based partition and mapper instances
/// to initialize any deferred domain and mapping functions they contain.
///
/// The initialize flag is provided to allow the invocation of the mapping
/// functions to be avoided in cases where it is known that they are not
/// instances of @ref deferred_map, specifically, the specification of
/// arbitrary distributions using containers of @ref arb_partition_info
/// instances.
///
/// @param spec View-based specification of container distribution that may
/// contain instances of @ref deferred_domain and @ref deferred_map
/// @param initialize flag indicating whether initialization of
/// @ref deferred_map instances should be performed
/// @return A reference to the instance passed to the function
///
/// This function is invoked inline in container constructors where the
/// partition and mapper instances are constructed before they're passed
/// to the distribution constructor.
//////////////////////////////////////////////////////////////////////
template <typename DistSpec>
std::shared_ptr<DistSpec>
initialize_deferred(std::shared_ptr<DistSpec> spec, bool initialize = true)
{
  if (initialize)
  {
    // Trigger initialization of deferred_map functor in partition view
    spec->mapfunc()->operator()(typename DistSpec::index_type());

    // Trigger initialization of deferred_map functor in mapping view
    spec->container().mapfunc()->operator()(
      typename DistSpec::view_container_type::index_type());
  }

  // Trigger initialization of deferred_domain in mapping view
  spec->container().domain().first();

  return spec;
}

} // namespace dist_spec_impl

//////////////////////////////////////////////////////////////////////
/// @brief Representation of a partition in the specification of an
/// arbitrary distribution.
///
/// The partition information required by the mapping functions used
/// in the view-based distribution specification is the range of GIDs
/// in the partition and the location to which the partition will be mapped.
/// The partition id is implicit and is the index of the partition info
/// in the container of partition information that is passed to
/// @ref arbitrary(View const& part_view,std::vector<location_type> const& locs)
//////////////////////////////////////////////////////////////////////
struct arbitrary_partition_info
{
private:
  unsigned long int m_first;
  unsigned long int m_last;
  unsigned int      m_loc;

public:
  arbitrary_partition_info()
    : m_first(-1), m_last(-1), m_loc(-1)
  { }

  arbitrary_partition_info(unsigned long int first, unsigned long int last,
                           unsigned int location)
    : m_first(first), m_last(last), m_loc(location)
  { }

  std::pair<unsigned long int, unsigned long int> domain(void) const
  { return std::make_pair(m_first, m_last); }

  unsigned int location(void) const
  { return m_loc; }

  void define_type(stapl::typer& t)
  {
    t.member(m_first);
    t.member(m_last);
    t.member(m_loc);
  }
};


STAPL_PROXY_HEADER(arbitrary_partition_info)
{
  STAPL_PROXY_DEFINES(arbitrary_partition_info)
  STAPL_PROXY_METHOD_RETURN(location, unsigned int)
  STAPL_PROXY_METHOD_RETURN_0(domain,
    STAPL_PROXY_CONCAT(std::pair<unsigned long int, unsigned long int>))
};



//////////////////////////////////////////////////////////////////////
/// @brief Distribution specification of nested containers.
///
/// In order to construct the elements of a nested container instance
/// with different distributions a specification of the distribution
/// for each element is needed. The class is constructed with a user
/// defined generator object whose operator[] receives the multi-dimensional
/// index of the element being constructed and returns the distribution
/// specification for the element.
//////////////////////////////////////////////////////////////////////
class composed_dist_spec;

namespace detail {
template<typename GID, typename Index>
struct balance_map_factory;
}
//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of a blocked distribution.
///
/// The GIDs will be blocked into partitions, and the partition ids are
/// balanced across the locations.
///
/// @param n Number of elements in the data to be distributed
/// @param block_size Number of elements in each partition
/// @param lvl Tag indicating the level of the system hierarchy across which
/// the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in a blocked distribution of GIDs.
/// @ingroup distribution_specs
//////////////////////////////////////////////////////////////////////
distribution_spec<>
block(unsigned long int n, unsigned long int block_size,
      level lvl = current_level);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of a blocked distribution
/// that will span a specific set of locations.
///
/// The GIDs will be blocked into partitions, and the partition ids are
/// balanced across the locations in the new communication group
/// constructed over the locations specified from the parent
/// communication group.
///
/// @param n Number of elements in the data to be distributed
/// @param block_size Number of elements in each partition
/// @param locs Explicit specification of the locations across
/// which the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in a blocked distribution of GIDs.
/// @ingroup distribution_specs
//////////////////////////////////////////////////////////////////////
distribution_spec<>
block(unsigned long int n, unsigned long int block_size,
      std::vector<location_type> const& locs);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of a cyclic distribution.
///
/// The GIDs will not be blocked.  Each single-element partition will be
/// mapped to a location in a round-robin manner beginning with location 0.
///
/// @param n Number of elements in the data to be distributed
/// @param lvl Tag indicating the level of the system hierarchy across which
/// the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in a cyclic distribution of GIDs.
/// @ingroup distribution_specs
///
//////////////////////////////////////////////////////////////////////
distribution_spec<>
cyclic(unsigned long int n, level lvl = current_level);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of a cyclic distribution
/// that will span a specific set of locations.
///
/// The GIDs will not be blocked.  Each single-element partition will be
/// mapped to a location in a round-robin manner beginning with location 0
/// in the new communication group constructed over the locations
/// specified from the parent communication group.
///
/// @param n Number of elements in the data to be distributed
/// @param locs Explicit specification of the locations across
/// which the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in a cyclic distribution of GIDs.
/// @ingroup distribution_specs
//////////////////////////////////////////////////////////////////////
distribution_spec<>
cyclic(unsigned long int n, std::vector<location_type> const& locs);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of a block-cyclic distribution.
///
/// The GIDs will be mapped into partitions of size @p block_size. Each
/// partition will be mapped to a location in a round-robin manner
/// beginning with location 0.
///
/// @param n Number of elements in the data to be distributed
/// @param block_size Number of elements in each partition
/// @param lvl Tag indicating the level of the system hierarchy across which
/// the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in a block-cyclic distribution of GIDs.
/// @ingroup distribution_specs
///
//////////////////////////////////////////////////////////////////////
distribution_spec<>
block_cyclic(unsigned long int n, unsigned long int block_size,
             level lvl = current_level);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of a block-cyclic distribution
/// that will span a specific set of locations.
///
/// The GIDs will be mapped into partitions of size @p block_size. Each
/// partition will be mapped to a location in a round-robin manner
/// beginning with location 0 in the new communication group constructed
/// over the locations specified from the parent communication group.
///
/// @param n Number of elements in the data to be distributed
/// @param block_size Number of elements in each partition
/// @param locs Explicit specification of the locations across
/// which the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in a block-cyclic distribution of GIDs.
/// @ingroup distribution_specs
//////////////////////////////////////////////////////////////////////
distribution_spec<>
block_cyclic(unsigned long int n, unsigned long int block_size,
             std::vector<location_type> const& locs);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of a balanced distribution.
///
/// The GIDs will be mapped into partitions of near even size. Each
/// of the @p num_locs partitions will be mapped to a unique location
/// beginning with location 0.
///
/// @param n Number of elements in the data to be distributed
/// @param lvl Tag indicating the level of the system hierarchy across which
/// the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in a balanced distribution of GIDs.
/// @ingroup distribution_specs
///
//////////////////////////////////////////////////////////////////////
distribution_spec<>
balance(unsigned long int n, level lvl = current_level);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of a balanced distribution
/// that will span a specific set of locations.
///
/// The GIDs will be mapped into partitions of near even size. Each
/// of the @p num_locs partitions will be mapped to a unique location
/// beginning with location 0 in the new communication group constructed
/// over the locations specified from the parent communication group.
///
/// @param n Number of elements in the data to be distributed
/// @param locs Explicit specification of the locations across
/// which the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in a balanced distribution of GIDs.
/// @ingroup distribution_specs
//////////////////////////////////////////////////////////////////////
distribution_spec<>
balance(unsigned long int n,
        std::vector<location_type> const& locs);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of an arbitrary distribution.
///
/// The mapping functions to map an element GID to a partition id and
/// a partition id to a location id are function objects that have the
/// appropriately defined function operators.  Given a functor to map
/// GIDS to partition ids of type @p GIDMap and a functor to map partition
/// ids to location ids of type @p PIDMap the interfaces required are:
/// @verbatim
/// unsigned long int GIDMap::operator()(unsigned long int) const;
/// stapl::location_type PIDMap::operator()(unsigned long int) const;
/// @endverbatim
///
/// @param n Number of elements in the data to be distributed
/// @param nparts Number of partitions the elements are mapped to as
/// part of the distribution process.
/// @param gid_to_pid Functor mapping element GIDs to partition ids
/// @param pid_to_lid Functor mapping partition ids to location ids
/// @param lvl Tag indicating the level of the system hierarchy across which
/// the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in the arbitrary distribution of GIDS specified by the mapping
/// functions provided.
/// @ingroup distribution_specs
///
//////////////////////////////////////////////////////////////////////
template <typename GIDMapFunc, typename PIDMapFunc>
distribution_spec<>
arbitrary(unsigned long int n, unsigned long int nparts,
          GIDMapFunc const &gid_to_pid,
          PIDMapFunc const &pid_to_lid,
          level lvl = current_level);

//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of an arbitrary distribution.
///
/// The mapping functions to map an element GID to a partition id and
/// a partition id to a location id are function objects that have the
/// appropriately defined function operators.  Given a functor to map
/// GIDS to partition ids of type @p GIDMap and a functor to map partition
/// ids to location ids of type @p PIDMap the interfaces required are:
/// @verbatim
/// unsigned long int GIDMap::operator()(gid_type) const;
/// stapl::location_type PIDMap::operator()(unsigned long int) const;
/// @endverbatim
///
/// @param dom The domain used for the container.
/// @param nparts Number of partitions the elements are mapped to as
/// part of the distribution process.
/// @param gid_to_pid Functor mapping element GIDs to partition ids
/// @param pid_to_lid Functor mapping partition ids to location ids
/// @param lvl Tag indicating the level of the system hierarchy across which
/// the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in the arbitrary distribution of GIDS specified by the mapping
/// functions provided.
/// @ingroup distribution_specs
///
/// @note The enable_if is here to help the compiler differentiate with
/// the arbitrary(unsigned long int n, unsigned long int nparts, ...) function
//////////////////////////////////////////////////////////////////////
template <typename Dom, typename GIDMapFunc, typename PIDMapFunc,
 typename = typename std::enable_if<!std::is_integral<Dom>::value>::type
>
distribution_spec<Dom>
arbitrary(Dom const& dom,
          unsigned long int nparts,
          GIDMapFunc const &gid_to_pid,
          PIDMapFunc const &pid_to_lid,
          level lvl = current_level);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of an arbitrary distribution
/// that will span a specific set of locations.
///
/// The mapping functions to map an element GID to a partition id and
/// a partition id to a location id are function objects that have the
/// appropriately defined function operators.  Given a functor to map
/// GIDS to partition ids of type @p GIDMap and a functor to map partition
/// ids to location ids of type @p PIDMap the interfaces required are:
/// @verbatim
/// unsigned long int GIDMap::operator()(unsigned long int) const;
/// stapl::location_type PIDMap::operator()(unsigned long int) const;
/// @endverbatim
///
/// The locations to which the partitions will be mapped begin with
/// location 0 in the new communication group constructed over the
/// locations specified from the parent communication group.
///
/// @param n Number of elements in the data to be distributed
/// @param nparts Number of partitions the elements are mapped to as
/// part of the distribution process.
/// @param gid_to_pid Functor mapping element GIDs to partition ids
/// @param pid_to_lid Functor mapping partition ids to location ids
/// @param locs Explicit specification of the locations across
/// which the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in the arbitrary distribution of GIDS specified by the mapping
/// functions provided.
/// @ingroup distribution_specs
//////////////////////////////////////////////////////////////////////
template <typename GIDMapFunc, typename PIDMapFunc>
distribution_spec<>
arbitrary(unsigned long int n, unsigned long int nparts,
          GIDMapFunc const &gid_to_pid,
          PIDMapFunc const &pid_to_lid,
          std::vector<location_type> const& locs);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of an arbitrary distribution.
///
/// @param part_view A view whose elements define the partitions of the
/// distribution and the location to which each one is mapped.
/// @param lvl Tag indicating the level of the system hierarchy across which
/// the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in the arbitrary distribution of GIDs specified by the partition
/// information provided.
///
/// Each element of the view can be an instance of
/// @ref arbitrary_partition_info.  If another type is used the interface it
/// must provide is:
/// @beginverbatim
/// std::pair<unsigned long int, unsigned long int> domain(void) const;
/// unsigned int location(void) const;
/// @endverbatim
///
/// These methods provide the information needed to construct each partition
/// in the container whose distribution is being specified.  The partition id
/// isn't explicitly provided.  Instead the index of the element in the
/// container is used as an implicit partition id.
/// @ingroup distribution_specs
///
//////////////////////////////////////////////////////////////////////
template <typename View>
distribution_spec<>
arbitrary(View const& part_view, level lvl = current_level);


//////////////////////////////////////////////////////////////////////
/// @brief Construct the specification of an arbitrary distribution
/// that will span a specific set of locations.
///
/// @param part_view A view whose elements define the partitions of the
/// distribution and the location to which each one is mapped.
/// @param locs Explicit specification of the locations across
/// which the data will be distributed.
/// @return A read-only view with the domains and mapping functions that
/// result in the arbitrary distribution of GIDs specified by the partition
/// information provided.
///
/// Each element of the view can be an instance of
/// @ref arbitrary_partition_info.  If another type is used the interface it
/// must provide is:
/// @beginverbatim
/// std::pair<unsigned long int, unsigned long int> domain(void) const;
/// unsigned int location(void) const;
/// @endverbatim
///
/// These methods provide the information needed to construct each partition
/// in the container whose distribution is being specified.  The partition id
/// isn't explicitly provided.  Instead the index of the element in the
/// container is used as an implicit partition id.
///
/// The locations to which the partitions will be mapped begin with
/// location 0 in the new communication group constructed over the
/// locations specified from the parent communication group.
/// @ingroup distribution_specs
//////////////////////////////////////////////////////////////////////
template <typename View>
distribution_spec<>
arbitrary(View const& part_view, std::vector<location_type> const& locs);

} // namespace stapl

#endif // STAPL_CONTAINERS_DISTRIBUTION_SPECIFICATIONS_FWD_HPP
