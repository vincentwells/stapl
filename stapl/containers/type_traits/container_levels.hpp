/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_TYPE_TRAITS_CONTAINER_LEVELS_HPP
#define STAPL_CONTAINERS_TYPE_TRAITS_CONTAINER_LEVELS_HPP

#include "is_container.hpp"

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine how many containers are nested within
/// the value_types of a given type.
///
/// For example, given the type <t>array<array<vector<int>>></tt>, this
/// metafunction would reflect a value of @c 3.
///
/// @tparam T The type in question
//////////////////////////////////////////////////////////////////////
template<typename T, bool = is_container<T>::value>
struct container_levels
{
  typedef unsigned int value_type;

  static const value_type value = 0;
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization of @ref container_levels for the case when @p T is a
///        container, effectively adding one more to the running total.
//////////////////////////////////////////////////////////////////////
template<typename T>
struct container_levels<T, true>
{
  typedef unsigned int value_type;

  static const value_type value =
    (1 + container_levels<typename T::value_type>::value);
};

} // namespace stapl

#endif
