/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GID_OF_FWD_HPP
#define STAPL_CONTAINERS_GID_OF_FWD_HPP

#include <boost/version.hpp>

namespace stapl{

template<typename Iter>
typename
  boost::unordered::iterator_detail::iterator<Iter>::value_type::first_type
gid_of(boost::unordered::iterator_detail::iterator<Iter> it);

template<typename Iter, typename Iter2>
typename
#if BOOST_VERSION < 105800
  boost::unordered::iterator_detail::c_iterator<Iter, Iter2>::value_type
gid_of(boost::unordered::iterator_detail::c_iterator<Iter, Iter2> it);
#else
  boost::unordered::iterator_detail::c_iterator<Iter>::value_type
gid_of(boost::unordered::iterator_detail::c_iterator<Iter> it);
#endif

} // namespace stapl

#endif // STAPL_CONTAINERS_GID_OF_FWD_HPP
