/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_CONTAINERS_GRAPH_FRONTIER_VIEW_HPP
#define STAPL_CONTAINERS_GRAPH_FRONTIER_VIEW_HPP

#include <stapl/views/core_view.hpp>
#include <stapl/views/mapping_functions/mapping_functions.hpp>
#include <stapl/utility/single_element_range.hpp>

namespace stapl {

template <typename PG,
          typename Frontier,
          typename Dom     = typename container_traits<PG>::domain_type,
          typename MapFunc = f_ident<typename Dom::index_type>,
          typename Derived = use_default>
class frontier_view;

template <typename C,
          typename Frontier,
          typename D,
          typename F,
          typename Derived>
struct view_traits<frontier_view<C, Frontier, D, F, Derived>>
  : default_view_traits<C, D, F, Derived>
{ };

//////////////////////////////////////////////////////////////////////
/// @brief View over a subset of a graph based on the set of active vertices.
///
/// @tparam PG The graph container
/// @tparam Frontier The frontier storing the set of active vertices
/// @tparam Dom The domain of the view
/// @tparam MapFunc The mapping function
/// @tparam Derived The most derived view
//////////////////////////////////////////////////////////////////////
template <typename PG,
          typename Frontier,
          typename Dom,
          typename MapFunc,
          typename Derived>
class frontier_view
  : public graph_view<PG,
                      Dom,
                      MapFunc,
                      frontier_view<PG, Frontier, Dom, MapFunc, Derived>>
{
  Frontier& m_frontier;

public:
  STAPL_VIEW_REFLECT_TRAITS(frontier_view)

  using base_type = graph_view<PG, Dom, MapFunc, frontier_view>;
  using partition_type = single_element_range<Frontier>;

  frontier_view(view_container_type* vcont, Frontier& frontier)
    : base_type(vcont), m_frontier(frontier)
  { }

  frontier_view(view_container_type const& vcont, Frontier& frontier)
    : base_type(vcont), m_frontier(frontier)
  { }

  partition_type partition()
  {
    return partition_type{m_frontier};
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Create a view over a subset of a graph based on the set
///        of active vertices.
///
/// @param graph The graph container
/// @param frontier The @p frontier / set of active vertices
//////////////////////////////////////////////////////////////////////
template <typename Graph, typename Frontier>
frontier_view<Graph, Frontier>
make_frontier_view(Graph const& graph, Frontier& frontier)
{
  return { graph, frontier };
}

} // namespace stapl

#endif
