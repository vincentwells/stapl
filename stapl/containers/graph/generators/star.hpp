/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_GENERATORS_STAR_HPP
#define STAPL_CONTAINERS_GRAPH_GENERATORS_STAR_HPP

#include <stapl/containers/graph/generators/generator.hpp>

namespace stapl {

namespace generators {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Functor which adds edges to form a star of the given dimensions.
/// @see make_star
//////////////////////////////////////////////////////////////////////
struct star_neighbors
{
  std::size_t m_root;
  bool m_bidirectional;

  typedef void result_type;

  //////////////////////////////////////////////////////////////////////
  /// @param root The descriptor of the vertex that will be the root of the star
  /// @param bidirectional Flag indicating that edges should be added in both
  ///                      directions.
  //////////////////////////////////////////////////////////////////////
  star_neighbors(std::size_t root, bool bidirectional)
    : m_root(root), m_bidirectional(bidirectional)
  { }

  template<typename Vertex, typename Graph>
  void operator()(Vertex v, Graph& view)
  {
    if (v.descriptor() != m_root)
    {
      view.add_edge_async(v.descriptor(), m_root);

      if (view.is_directed() && m_bidirectional)
        view.add_edge_async(m_root, v.descriptor());
    }
  }

  void define_type(typer& t)
  {
    t.member(m_root);
    t.member(m_bidirectional);
  }
};

} // namespace detail

//////////////////////////////////////////////////////////////////////
/// @brief Generates a star graph of size n, with root r.
///
/// The star graph will have the property that all of the vertices have an edge
/// to a single particular vertex, designated the root. If the graph generated
/// is not bidirectional, all non-root vertices will have a directed edge
/// to the root and the root will have no outgoing edges.
///
/// This function mutates the input graph.
///
/// @param g A view over the graph to generate.
/// @param n Size of the star.
/// @param r The descriptor of the vertex that is the root.
/// @param bidirectional Edges should be added in both directions
///
/// @return The original view, now containing the generated graph.
//////////////////////////////////////////////////////////////////////
template <typename GraphView>
GraphView make_star(GraphView& g, std::size_t n, std::size_t r,
                    bool bidirectional = true)
{
  typedef typename detail::star_neighbors ef_t;

  return make_generator<GraphView, ef_t>(g, n, ef_t(r, bidirectional))();
}


//////////////////////////////////////////////////////////////////////
/// @brief Generates a star graph of size n, with root r.
///
/// The star graph will have the property that all of the vertices have an edge
/// to a single particular vertex, designated the root. If the graph generated
/// is not bidirectional, all non-root vertices will have a directed edge
/// to the root and the root will have no outgoing edges.
///
/// The returned view owns its underlying container.
///
/// @param n Size of the star.
/// @param r The descriptor of the vertex that is the root.
/// @param bidirectional Edges should be added in both directions
///
/// @return The original view, now containing the generated graph.
///
/// @b Example
/// @snippet star.cc Example
//////////////////////////////////////////////////////////////////////
template <typename GraphView>
GraphView make_star(std::size_t n, std::size_t r, bool bidirectional = true)
{
  typedef typename detail::star_neighbors ef_t;

  return make_generator<GraphView, ef_t>(n, ef_t(r, bidirectional))();
}

} // namespace generators

} // namespace stapl

#endif
