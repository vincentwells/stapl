/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_GENERATORS_LISTS_TREE_HPP
#define STAPL_CONTAINERS_GRAPH_GENERATORS_LISTS_TREE_HPP

#include <stapl/containers/graph/generators/generator.hpp>

namespace stapl {

namespace generators {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Functor which adds edges to form a tree-of-lists graph of the given
///   size.
//////////////////////////////////////////////////////////////////////
struct lists_tree_neighbors
{
  size_t m_x;
  size_t m_y;
  bool m_bidirectional;

  typedef void result_type;

  //////////////////////////////////////////////////////////////////////
  /// @param nx Number of branches in the tree.
  /// @param ny Number of vertices in each list.
  /// @param bidirectional True for adding back-edges, False for forward only.
  //////////////////////////////////////////////////////////////////////
  lists_tree_neighbors(size_t nx, size_t ny, bool bidirectional)
    : m_x(nx), m_y(ny), m_bidirectional(bidirectional)
  { }

  template<typename Vertex, typename Graph>
  void operator()(Vertex v, Graph& view)
  {
    size_t i = v.descriptor();
    for (size_t j = 0; j < m_y; ++j) {
      size_t s = (i*m_y) + j;
      size_t t = (((i+1))*m_y) + j;

      if (t < m_x*m_y) {
        view.add_edge_async(s, t);
        if (view.is_directed() && m_bidirectional)
          view.add_edge_async(t, s);
      }
    }
  }

  void define_type(typer& t)
  {
    t.member(m_x);
    t.member(m_y);
    t.member(m_bidirectional);
  }
};

}


//////////////////////////////////////////////////////////////////////
/// @brief Generate a tree-of-lists graph.
///
/// The generated graph is a tree of x branches, each
/// of which is a list containing y vertices.
///
/// This function mutates the input graph.
///
/// @param g A view over the graph to generate.
/// @param nx Number of branches in the tree.
/// @param ny Number of vertices in each list.
/// @param bidirectional True for adding back-edges, False for forward only.
/// @return The original view, now containing the generated graph.
//////////////////////////////////////////////////////////////////////
template <typename GraphView>
GraphView make_lists_tree(GraphView& g, size_t nx, size_t ny,
                          bool bidirectional=true)
{
  typedef typename detail::lists_tree_neighbors ef_t;
  GraphView vw =
    make_generator<GraphView, ef_t>(g, nx*ny, ef_t(nx, ny, bidirectional))();

  if (get_location_id() == 0)
    for (size_t i = 0; i < ny-1; ++i)
      vw.add_edge_async(i, i+1);

  return vw;
}

//////////////////////////////////////////////////////////////////////
/// @brief Generate a tree-of-lists graph.
///
/// The generated graph is a tree of x branches, each
/// of which is a list containing y vertices.
///
/// The returned view owns its underlying container.
///
/// @param nx Number of branches in the tree.
/// @param ny Number of vertices in each list.
/// @param bidirectional True for adding back-edges, False for forward only.
/// @return A view over the generated graph.
///
/// @b Example
/// @snippet lists_tree.cc Example
/////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
template <typename GraphView>
GraphView make_lists_tree(size_t nx, size_t ny, bool bidirectional=true)
{
  typedef typename detail::lists_tree_neighbors ef_t;
  GraphView vw =
    make_generator<GraphView, ef_t>(nx*ny, ef_t(nx, ny, bidirectional))();

  if (get_location_id() == 0)
    for (size_t i = 0; i < ny-1; ++i)
      vw.add_edge_async(i, i+1);

  return vw;
}

} // namespace generators

} // namespace stapl

#endif
