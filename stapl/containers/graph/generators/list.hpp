/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_GENERATORS_LIST_HPP
#define STAPL_CONTAINERS_GRAPH_GENERATORS_LIST_HPP

#include <stapl/containers/graph/generators/generator.hpp>

namespace stapl {

namespace generators {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Functor which adds edges to form a list graph.
//////////////////////////////////////////////////////////////////////
struct list_neighbors
{
  size_t m_n;
  size_t m_start;
  bool   m_bidirectional;

  typedef void result_type;

  //////////////////////////////////////////////////////////////////////
  /// @param n Size of the graph.
  /// @param start First vertex id of the list.
  /// @param bidirectional True for adding back-edges, False for forward only.
  //////////////////////////////////////////////////////////////////////
  list_neighbors(size_t n, size_t start, bool bidirectional)
    : m_n(n), m_start(start), m_bidirectional(bidirectional)
  { }

  template<typename Vertex, typename Graph>
  void operator()(Vertex v, Graph& view)
  {
    if (v.descriptor() < m_start+m_n-1 && v.descriptor() >= m_start) {
      view.add_edge_async(v.descriptor(), v.descriptor()+1);
      if (view.is_directed() && m_bidirectional)
        view.add_edge_async(v.descriptor()+1, v.descriptor());
    }
  }

  void define_type(typer& t)
  {
    t.member(m_n);
    t.member(m_start);
    t.member(m_bidirectional);
  }
};

}


//////////////////////////////////////////////////////////////////////
/// @brief Generate a list graph.
///
/// The generated list will contain n vertices, each with an edge to its
/// next element in the list (except for the last).
///
/// This function mutates the input graph.
///
/// @param g A view over the graph to generate.
/// @param n The number of nodes in the list.
/// @param bidirectional True for adding back-edges, False for forward only.
/// @return The original view, now containing the generated graph.
/////////////////////////////////////////////////////////////////////////
template <typename GraphView>
GraphView make_list(GraphView& g, size_t n, bool bidirectional=true)
{
  typedef typename detail::list_neighbors     ef_t;
  return make_generator<GraphView, ef_t>(g, n, ef_t(n, 0, bidirectional))();
}


//////////////////////////////////////////////////////////////////////
/// @brief Generate a list graph.
///
/// The generated list will contain n vertices, each with an edge to its
/// next element in the list (except for the last).
///
/// The returned view owns its underlying container.
///
/// @param n The number of nodes in the list.
/// @param bidirectional True for adding back-edges, False for forward only.
/// @return A view over the generated graph.
///
/// @b Example
/// @snippet list.cc Example
/////////////////////////////////////////////////////////////////////////
template <typename GraphView>
GraphView make_list(size_t n, bool bidirectional=true)
{
  typedef typename detail::list_neighbors     ef_t;
  return make_generator<GraphView, ef_t>(n, ef_t(n, 0, bidirectional))();
}

} // namespace generators

} // namespace stapl

#endif
