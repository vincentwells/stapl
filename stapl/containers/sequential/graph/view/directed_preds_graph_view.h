/*
 // Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
 // component of the Texas A&M University System.

 // All rights reserved.

 // Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STAPL_CONTAINERS_SEQUENTIAL_GRAPH_DIRECTED_PREDS_GRAPH_VIEW_HPP
#define STAPL_CONTAINERS_SEQUENTIAL_GRAPH_DIRECTED_PREDS_GRAPH_VIEW_HPP

#include <stapl/utility/use_default.hpp>

#include "graph_view_property_adaptor.h"

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief View providing predecessor info for graphs.
/// Specialization when the underlying view/graph is DPG.
/// @ingroup graph
///
/// In general there are two cases; one where this view is defined over
/// a DPG graph, in which case the view is just a wrapper; and second
/// where the underlying graph/view is not DPG and then we have to keep
/// extra storage to keep track of predecessors in the view separate
/// from the graph.
/// These two cases are implemented using different partial specializations.
//////////////////////////////////////////////////////////////////////
template <class Graph, class VAdaptor = use_default,
    class EAdaptor = use_default>
class dpg_view
  : public dynamic_graph_view<Graph, VAdaptor, EAdaptor>
{
  typedef dynamic_graph_view<Graph, VAdaptor, EAdaptor> base_type;
  typedef dpg_view<Graph, VAdaptor, EAdaptor> this_type;
public:
  typedef typename base_type::vertex_descriptor vertex_descriptor;
  typedef typename base_type::edge_descriptor edge_descriptor;

  typedef typename base_type::vertex_iterator vertex_iterator;
  typedef typename base_type::const_vertex_iterator const_vertex_iterator;
  typedef typename base_type::adj_edge_iterator adj_edge_iterator;
  typedef typename base_type::const_adj_edge_iterator const_adj_edge_iterator;
  typedef typename base_type::edge_iterator edge_iterator;
  typedef typename base_type::const_edge_iterator const_edge_iterator;

  typedef typename vertex_iterator::property_type vertex_property;
  typedef typename adj_edge_iterator::property_type edge_property;

  //new relative to dynamic view
  typedef typename Graph::preds_iterator preds_iterator;
  typedef typename Graph::const_preds_iterator const_preds_iterator;

  //////////////////////////////////////////////////////////////////////
  /// @brief Create a dpg view over a dpg graph.
  //////////////////////////////////////////////////////////////////////
  dpg_view(Graph& g)
    : base_type(g)
  {
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Set the graph for the lazy-update mode.
  //////////////////////////////////////////////////////////////////////
  void set_async_update(void)
  {
    this->m_g.set_async_update();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Set the graph for the always update mode.
  //////////////////////////////////////////////////////////////////////
  void set_sync_update(void)
  {
    this->m_g.set_sync_update();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief When in the lazy update mode this method should be called to
  /// update all predecessors.
  //////////////////////////////////////////////////////////////////////
  void set_predecessors(void)
  {
    this->m_g.set_predecessors();
  }
};


}//end namespace stapl

#endif
