/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_PARTITIONS_GENERATOR_HPP
#define STAPL_CONTAINERS_PARTITIONS_GENERATOR_HPP

#include <stapl/runtime.hpp>
#include <stapl/utility/tuple.hpp>

namespace stapl {

namespace partitions_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Function object that initializes an n-dimensional
/// tuple of partitions to  n balanced partitions, each partitioned by
/// the number of locations.
/// @tparam I The dimension of the partition that is currently being
/// populated.
/// @tparam Domain The domain that is to be partitioned
/// @tparam Tuple The type of the tuple to populate
//////////////////////////////////////////////////////////////////////
template<int I, typename Domain, typename Tuple>
struct partitions_generator
{
  Domain m_domain;
  location_type m_nlocs;

  partitions_generator(Domain const& domain)
    : m_domain(domain), m_nlocs(get_num_locations())
  { }

  partitions_generator(Domain const& domain, location_type nlocs)
    : m_domain(domain), m_nlocs(nlocs)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Function operator to initialize the tuple.
  /// @param t The tuple of partitions that is to be initialized.
  /// @todo This can possibly be optimized to pass-by-const ref if we don't
  /// discard qualifiers somewhere down the chain
  //////////////////////////////////////////////////////////////////////
  Tuple operator()(Tuple t)
  {
    typedef typename tuple_element<I-1, Tuple>::type   dom_t;

    get<I-1>(t) = dom_t(m_domain.template get_domain<I-1>(),
                        m_nlocs);

    return partitions_generator<I-1, Domain, Tuple>(m_domain, m_nlocs)(t);
  }

  /////////////////////////////////////////////////////////////////////
  /// @brief Function operator to initialize the tuple.
  /// @param t The tuple of partitions that is to be initialized.
  /// @param nparts The number of partitions to generate in this dimension
  /// @todo This can possibly be optimized to pass-by-const ref if we don't
  /// discard qualifiers somewhere down the chain.
  //////////////////////////////////////////////////////////////////////
  template <typename NParts>
  Tuple operator()(Tuple t, NParts nparts)
  {
    size_t p = get<I-1>(nparts);
    typedef typename tuple_element<I-1, Tuple>::type   dom_t;

    get<I-1>(t) = dom_t(m_domain.template get_domain<I-1>(), p);

    return
      partitions_generator<I-1, Domain, Tuple>(m_domain, m_nlocs)(t, nparts);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Base case to end recursion for @ref partitions_generator
//////////////////////////////////////////////////////////////////////
template<typename Domain, typename Tuple>
struct partitions_generator<1, Domain, Tuple>
{
  Domain m_domain;
  location_type m_nlocs;

  partitions_generator(Domain const& domain)
    : m_domain(domain), m_nlocs(get_num_locations())
  { }

  partitions_generator(Domain const& domain, location_type nlocs)
    : m_domain(domain), m_nlocs(nlocs)
  { }

  Tuple operator()(Tuple t)
  {
    typedef typename tuple_element<0, Tuple>::type   dom_t;
    get<0>(t) = dom_t(m_domain.template get_domain<0>(), m_nlocs);
    return t;
  }

  template <typename NParts>
  Tuple operator()(Tuple t, NParts nparts)
  {
    typedef typename tuple_element<0, Tuple>::type dom_t;
    get<0>(t) = dom_t(m_domain.template get_domain<0>(), get<0>(nparts));
    return t;
  }
};

} // namespace partitions_impl

} // namespace stapl

#endif // STAPL_CONTAINERS_PARTITIONS_GENERATOR_HPP
