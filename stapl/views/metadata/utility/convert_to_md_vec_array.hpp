/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_UTILITY_CONVERT_TO_MD_VEC_ARRAY_HPP
#define STAPL_VIEWS_METADATA_UTILITY_CONVERT_TO_MD_VEC_ARRAY_HPP

#include <stapl/views/metadata/container/growable_fwd.hpp>
#include <stapl/views/metadata/container/metadata_view.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Defines the coarsen metadata view type for the given
///        container type.
///
/// The metadata container type used is @ref metadata::growable_container.
/// @tparam MDCont Metadata container type.
//////////////////////////////////////////////////////////////////////
template<typename MDCont>
struct convert_to_md_vec_array
{
  typedef typename MDCont::second_type::value_type      value_type;
  typedef metadata::growable_container<value_type>      part_type;
  typedef metadata::view<part_type>                     type;
};

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_UTILITY_CONVERT_TO_MD_VEC_ARRAY_HPP
