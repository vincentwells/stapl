/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_FUNCTOR_VIEW_HPP
#define STAPL_VIEWS_FUNCTOR_VIEW_HPP

#include <stapl/views/array_view.hpp>
#include <stapl/containers/generators/functor.hpp>

#include <iostream>

namespace stapl {


//////////////////////////////////////////////////////////////////////
/// @brief Defines a nested trait type that is the type of a functor view
/// parameterized with a functor type
//////////////////////////////////////////////////////////////////////
template <typename Func, int n>
struct functor_view_type
{
  typedef array_view<functor_container<Func, 1>> type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper function that creates an array view on top of a
///        functor container.
/// @param size Number of elements in the container.
/// @param func Functor to be called on each [] operator call.
//////////////////////////////////////////////////////////////////////
template <typename Func>
typename functor_view_type<Func, 1>::type
functor_view(std::size_t size, Func const& func)
{
  return typename functor_view_type<Func, 1>::type(
                    new functor_container<Func, 1>(size, func));
}

} // stapl namespace

#endif // STAPL_VIEWS_FUNCTOR_VIEW_HPP
