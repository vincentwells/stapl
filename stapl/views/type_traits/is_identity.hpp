/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_TYPE_TRAITS_IS_IDENTITY_HPP
#define STAPL_VIEWS_TYPE_TRAITS_IS_IDENTITY_HPP

#include <boost/mpl/bool.hpp>
#include <stapl/views/mapping_functions/identity.hpp>
#include <stapl/views/type_traits/is_view.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Type checker to determine if a mapping function is an
///        identity mapping function.
//////////////////////////////////////////////////////////////////////
template <typename MF>
struct is_identity
  : boost::mpl::false_
{ };


template <typename T>
struct is_identity<f_ident<T> >
  : boost::mpl::true_
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if a view has an identity mapping
///        function. Differs from @c has_identity in that if the type
///        is not a view, it defaults to false_type.
//////////////////////////////////////////////////////////////////////
template<typename View, bool IsView = is_view<View>::type::value>
struct has_identity_mf
: public std::false_type
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Specialization for views
//////////////////////////////////////////////////////////////////////
template<typename View>
struct has_identity_mf<View, true>
: public is_identity<typename view_traits<View>::map_function>::type
{ };

} // stapl namespace

#endif /* STAPL_VIEWS_TYPE_TRAITS_IS_IDENTITY_HPP */
