/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEW_HANDLE_ACCESSOR_HPP
#define STAPL_VIEW_HANDLE_ACCESSOR_HPP

#include <stapl/runtime.hpp>
#include <stapl/utility/invoke_arg.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Defines an accessor over a p_object, keeping a rmi_handle
///        reference to perform rmi communications.
/// @tparam T Type of object (derived) from @ref p_object.
/// @todo Use proper broadcast primitives.
//////////////////////////////////////////////////////////////////////
template <typename T>
struct handle_accessor
{
private:
  typedef rmi_handle::reference         ref_t;

  T*                                    m_t_ptr;
  /* const */ ref_t                     m_handle_ref;

public:
  typedef T value_type;

  handle_accessor(T* t_ptr)
    : m_t_ptr(t_ptr),
      m_handle_ref(t_ptr->get_rmi_handle())
  { }

  handle_accessor(ref_t const& ref)
    : m_t_ptr(NULL),
      m_handle_ref(ref)
  { }

  void define_type(typer&)
  {
    stapl_assert(0, "handle_accessor isn't packable at the moment");
  }

  template<typename Class, typename... Args>
  void invoke(void (Class::* const memberFuncPtr)(Args...),
              typename invoke_arg<Args>::type const&... args)

  {
    if (m_t_ptr)
      (m_t_ptr/*.get()*/->*memberFuncPtr)(args...);
    else
      async_rmi(all_locations, m_handle_ref, memberFuncPtr, args...);
  }

  template<typename Class, typename Rtn, typename... Args>
  Rtn invoke(Rtn (Class::* const memberFuncPtr)(Args...),
             typename invoke_arg<Args>::type const&... args)
  {
    if (m_t_ptr)
      return (m_t_ptr->*memberFuncPtr)(args...);
    else
      return restore(m_handle_ref, memberFuncPtr, args...).get();
  }

  template<typename Class, typename Rtn, typename... Args>
  Rtn const_invoke(Rtn (Class::* const memberFuncPtr)(Args...) const,
                   typename invoke_arg<Args>::type const&... args)
  {
    if (m_t_ptr)
      return (m_t_ptr->*memberFuncPtr)(args...);
    else
      return restore(m_handle_ref, memberFuncPtr, args...).get();
  }
}; // class handle_accessor

} // namespace stapl

#endif // ifndef STAPL_VIEW_HANDLE_ACCESSOR_HPP
