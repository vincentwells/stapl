/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_CONCURRENCY_MUTEX_HPP
#define STAPL_RUNTIME_CONCURRENCY_MUTEX_HPP

#include "config.hpp"
#include "../config/platform.hpp"
#include <atomic>
#include <mutex>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Spin-based synchronization primitive that does not emit memory
///        fences.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
class thin_spin_mutex
{
public:
  typedef std::atomic_flag& native_handle_type;

private:
  STAPL_RUNTIME_CACHELINE_ALIGNED std::atomic_flag m_lock;

public:
  thin_spin_mutex(void) noexcept
  { m_lock.clear(std::memory_order_relaxed); }

  void lock(void)
  {
    while (!try_lock())
      ; // spin until lock acquired
  }

  bool try_lock(void)
  { return !m_lock.test_and_set(std::memory_order_relaxed); }

  void unlock(void)
  { m_lock.clear(std::memory_order_relaxed); }

  native_handle_type native_handle(void) noexcept
  { return m_lock; }
};


//////////////////////////////////////////////////////////////////////
/// @brief Hierarchical synchronization primitive that does not emit memory
///        fences.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
class hierarchical_mutex
{
private:
  thin_spin_mutex           m_lock;
  hierarchical_mutex* const m_parent;

public:
  explicit hierarchical_mutex(hierarchical_mutex* const p = nullptr) noexcept
  : m_parent(p)
  { }

  void lock(void)
  {
    while (!m_lock.try_lock())
      ; // spin until lock acquired

    if (m_parent)
      m_parent->lock();
  }

  bool try_lock(void)
  {
    if (!m_lock.try_lock())
      return false;

    if (m_parent) {
      if (!m_parent->try_lock()) {
        m_lock.unlock();
        return false;
      }
    }
    return true;
  }

  void unlock(void)
  {
    m_lock.unlock();
    if (m_parent)
      m_parent->unlock();
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Tag type for read lock ownership.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
struct read_lock_t
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Tag for read lock ownership.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
constexpr read_lock_t read_lock = { };


//////////////////////////////////////////////////////////////////////
/// @brief Tag type for write lock ownership.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
struct write_lock_t
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Tag for write lock ownership.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
constexpr write_lock_t write_lock = { };


//////////////////////////////////////////////////////////////////////
/// @brief Lock guard for read lock ownership.
///
/// If write lock is required, then @c lock_guard should be used.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
template<typename Mutex>
struct read_lock_guard
{
  Mutex& m_mtx;

  read_lock_guard(Mutex& mtx)
  : m_mtx(mtx)
  { m_mtx.lock(read_lock); }

  ~read_lock_guard(void)
  { m_mtx.unlock(); }
};

} // namespace runtime

} // namespace stapl


#if defined(STAPL_RUNTIME_TBB_AVAILABLE)
// use TBB optimized read-write mutex
# include "tbb/read_write_mutex.hpp"
#elif defined(STAPL_RUNTIME_WINDOWS_TARGET)
// use generic implementation
# include "generic/read_write_mutex.hpp"
#else
// pthreads should be available, use pthreads-based read-write lock
# include "pthread/read_write_mutex.hpp"
#endif

#endif
