/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_CONCURRENCY_GENERIC_CENTRALIZED_FUZZY_BARRIER_HPP
#define STAPL_RUNTIME_CONCURRENCY_GENERIC_CENTRALIZED_FUZZY_BARRIER_HPP

#include "../config.hpp"
#include "../../exception.hpp"
#include <atomic>
#include <cstddef>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Centralized fuzzy barrier.
///
/// @ingroup concurrency
//////////////////////////////////////////////////////////////////////
class centralized_fuzzy_barrier
{
public:
  typedef std::size_t size_type;

private:
  /// Total number of threads.
  const size_type                                        m_nth;
  /// Sense flag for barrier release.
  STAPL_RUNTIME_CACHELINE_ALIGNED std::atomic<bool>      m_flag;
  /// Number of threads not yet arrived to the barrier.
  STAPL_RUNTIME_CACHELINE_ALIGNED std::atomic<size_type> m_count;

public:
  explicit centralized_fuzzy_barrier(const size_type nth = 1) noexcept
  : m_nth(nth),
    m_flag(false),
    m_count(nth)
  { }

  template<typename Function>
  void wait(const size_type /*tid*/, Function&& f)
  {
    const bool flag = m_flag.load();
    if (--m_count==0) {
      m_count = m_nth;
      m_flag  = !flag;
    }
    else {
      while (flag==m_flag.load())
        f();
    }
  }
};


typedef centralized_fuzzy_barrier fuzzy_barrier;

} // namespace runtime

} // namespace stapl

#endif
