/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_P_OBJECT_REGISTRY_HPP
#define STAPL_RUNTIME_P_OBJECT_REGISTRY_HPP

#include "rmi_handle_fwd.hpp"
#include <typeinfo>

namespace stapl {

namespace runtime {

class p_object_registry
{
public:
  //////////////////////////////////////////////////////////////////////
  /// @brief Associates handle @p h with @p p of type @p t.
  //////////////////////////////////////////////////////////////////////
  static void register_object(const rmi_handle* h,
                              const void* p,
                              std::type_info const& t);

  //////////////////////////////////////////////////////////////////////
  /// @brief Disassociates handle @p h from its registered object.
  //////////////////////////////////////////////////////////////////////
  static void unregister_object(const rmi_handle* h);

  //////////////////////////////////////////////////////////////////////
  /// @brief Verifies that @p p can be cast to type @p t.
  //////////////////////////////////////////////////////////////////////
  static void verify_object_type(const void* p, std::type_info const& t);
};

} // namespace runtime

} // namespace stapl

#endif
