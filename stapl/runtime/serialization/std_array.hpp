/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_SERIALIZATION_STD_ARRAY_HPP
#define STAPL_RUNTIME_SERIALIZATION_STD_ARRAY_HPP

#include "typer_fwd.hpp"
#include <type_traits>

namespace std {

template<typename T, std::size_t Size>
struct array;

} // namespace std

namespace boost {

template<typename T, std::size_t Size>
class array;

} // namespace boost


namespace stapl {

////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref define_type_provider for @c std::array.
///
/// @ingroup serialization
////////////////////////////////////////////////////////////////////
template<typename T, std::size_t Size>
struct define_type_provider<std::array<T, Size>>
{
  //////////////////////////////////////////////////////////////////////
  /// @brief @c std::array doppelganger that provides @c define_type().
  //////////////////////////////////////////////////////////////////////
  struct wrapper
  : public std::array<T, Size>
  {
    void define_type(typer& t)
    {
      for (std::size_t i = 0; i < Size; ++i)
        t.member(this->operator[](i));
    }
  };

  static constexpr wrapper& apply(std::array<T, Size>& t) noexcept
  {
    return static_cast<wrapper&>(t);
  }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref define_type_provider for @c boost::array.
///
/// @ingroup serialization
////////////////////////////////////////////////////////////////////
template<typename T, std::size_t Size>
struct define_type_provider<boost::array<T, Size>>
{
  //////////////////////////////////////////////////////////////////////
  /// @brief @c boost::array doppelganger that provides @c define_type().
  //////////////////////////////////////////////////////////////////////
  struct wrapper
  : public boost::array<T, Size>
  {
    void define_type(typer& t)
    {
      for (std::size_t i = 0; i < Size; ++i)
        t.member(this->operator[](i));
    }
  };

  static constexpr wrapper& apply(boost::array<T, Size>& t) noexcept
  {
    return static_cast<wrapper&>(t);
  }
};

} // namespace stapl

#endif
