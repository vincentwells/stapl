/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_INSTRUMENTATION_NO_COMM_GUARD_HPP
#define STAPL_RUNTIME_INSTRUMENTATION_NO_COMM_GUARD_HPP

#include "../exception.hpp"
#include "../primitive_traits.hpp"
#include "../this_context.hpp"
#include <mutex>
#include <unordered_map>
#include <utility>

namespace stapl {

////////////////////////////////////////////////////////////////////
/// @brief Registers the given context as a no communication section of code.
///
/// If any communication happens within the scope of a @ref no_comm_guard
/// object, then the execution aborts with an error message.
///
/// @ref no_comm_guard is used during development to guarantee that a piece of
/// code never does any kind of unexpected communication.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
class no_comm_guard
{
public:
  typedef runtime::context_id                        key_type;
private:
  typedef std::unordered_map<key_type, unsigned int> container_type;

  ////////////////////////////////////////////////////////////////////
  /// @brief Returns the container of registered objects and the container's
  ///        mutex.
  ////////////////////////////////////////////////////////////////////
  static std::pair<container_type, std::mutex>& get_container(void)
  {
    static std::pair<container_type, std::mutex> map;
    return map;
  }

  const key_type m_key;

public:
  ////////////////////////////////////////////////////////////////////
  /// @brief Returns @c true if a @ref no_comm_guard is enabled for the given
  ///        context id.
  ////////////////////////////////////////////////////////////////////
  static bool enabled(key_type const& key)
  {
    auto& r = get_container();
    auto& c = r.first;

    std::lock_guard<std::mutex> lock{r.second};
    return (c.find(key)!=c.end());
  }

  ////////////////////////////////////////////////////////////////////
  /// @brief Enables the no-communication checking for the current context.
  ////////////////////////////////////////////////////////////////////
  no_comm_guard(void)
  : m_key(runtime::this_context::get_id())
  {
    auto& r = get_container();
    auto& c = r.first;

    std::lock_guard<std::mutex> lock{r.second};
    ++(c[m_key]);
  }

  no_comm_guard(no_comm_guard const&) = delete;
  no_comm_guard& operator=(no_comm_guard const&) = delete;

  ////////////////////////////////////////////////////////////////////
  /// @brief Disables the no-communication checking for the current context.
  ////////////////////////////////////////////////////////////////////
  ~no_comm_guard(void)
  {
    auto& r = get_container();
    auto& c = r.first;

    std::lock_guard<std::mutex> lock{r.second};
    auto it = c.find(m_key);
    STAPL_RUNTIME_ASSERT(it!=c.end() && (it->second)>0);
    if (--(it->second)==0)
      c.erase(it);
  }
};

} // namespace stapl


////////////////////////////////////////////////////////////////////
/// @brief Detects if a @ref stapl::no_comm_guard is enabled and a section of
///        code does communication.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
# define STAPL_RUNTIME_CALL_NO_COMM_GUARD(traits)                            \
{                                                                            \
  if ((traits & stapl::runtime::primitive_traits::comm) &&                   \
      stapl::no_comm_guard::enabled(stapl::runtime::this_context::get_id())) \
    stapl::runtime::warning(                                                 \
      "STAPL warning: Communication while stapl::no_comm_guard active",      \
      BOOST_CURRENT_FUNCTION);                                               \
}

#endif
