/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_INSTRUMENTATION_INSTRUMENT_HPP
#define STAPL_RUNTIME_INSTRUMENTATION_INSTRUMENT_HPP

#include "../utility/option.hpp"

namespace stapl {

namespace runtime {

////////////////////////////////////////////////////////////////////
/// @brief Provides instrumentation capabilities for runtime functions.
///
/// This is the in-house instrumentation tool that allows accumulation of values
/// using a key.
///
/// The default output file has the name @c stapl_instrumentation.pid where
/// @p pid is the process id. The name can be overridden with the environment
/// variable @c STAPL_INSTRUMENTATION_FILE.
///
/// @ingroup instrumentationImpl
////////////////////////////////////////////////////////////////////
class instrument
{
public:
  ////////////////////////////////////////////////////////////////////
  /// @brief Initializes instrumentation.
  ////////////////////////////////////////////////////////////////////
  static void initialize(option const&);

  ////////////////////////////////////////////////////////////////////
  /// @brief Finalizes instrumentation and outputs everything to the log file.
  ////////////////////////////////////////////////////////////////////
  static void finalize(void);

  ////////////////////////////////////////////////////////////////////
  /// @brief Increases the counter for the given key.
  ////////////////////////////////////////////////////////////////////
  static void accumulate(const char*);

  ////////////////////////////////////////////////////////////////////
  /// @brief Pushes back the value for the given key.
  ////////////////////////////////////////////////////////////////////
  static void push_back(const char*, int);

  ////////////////////////////////////////////////////////////////////
  /// @brief Clears all accumulated data.
  ////////////////////////////////////////////////////////////////////
  static void clear(void);
};

} // namespace runtime

} // namespace stapl


////////////////////////////////////////////////////////////////////
/// @brief Calls the @ref stapl::runtime::instrument::accumulate() with the
///        given arguments.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
#define STAPL_RUNTIME_CALL_INSTRUMENT(s) \
 stapl::runtime::instrument::accumulate((s));


////////////////////////////////////////////////////////////////////
/// @brief Calls the @ref stapl::runtime::instrument::push_back() with the given
///        arguments.
///
/// @ingroup instrumentation
////////////////////////////////////////////////////////////////////
#define STAPL_RUNTIME_STATISTICS(s, n) \
 stapl::runtime::instrument::push_back((s),(n))

#endif
