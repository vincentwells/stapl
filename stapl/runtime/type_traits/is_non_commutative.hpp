/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_TYPE_TRAITS_IS_NON_COMMUTATIVE_HPP
#define STAPL_RUNTIME_TYPE_TRAITS_IS_NON_COMMUTATIVE_HPP

#include <type_traits>
#include <utility>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Wrapper class for a non-commutative binary operator.
///
/// @related non_commutative()
/// @see non_commutative
/// @ingroup ARMITags
//////////////////////////////////////////////////////////////////////
template<typename BinaryOperation>
class non_commutative_wrapper
: public BinaryOperation
{
public:
  template<typename T>
  explicit non_commutative_wrapper(T&& op) noexcept
  : BinaryOperation(std::forward<T>(op))
  { }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref non_commutative_wrapper for non-commutative
///        functions.
///
/// @related non_commutative()
/// @see non_commutative
/// @ingroup ARMITags
//////////////////////////////////////////////////////////////////////
template<typename R, typename T1, typename T2>
class non_commutative_wrapper<R(T1, T2)>
{
private:
  using fun_ptr_type = R(*)(T1, T2);

  fun_ptr_type m_f;

public:
  constexpr explicit non_commutative_wrapper(R (*f)(T1, T2)) noexcept
  : m_f(f)
  { }

  operator fun_ptr_type(void) const noexcept
  { return m_f; }
};


//////////////////////////////////////////////////////////////////////
/// @brief Declares binary operator @p op as non-commutative.
///
/// @see is_non_commutative
/// @related non_commutative_wrapper
/// @ingroup ARMITags
//////////////////////////////////////////////////////////////////////
template<typename BinaryOperation>
constexpr non_commutative_wrapper<typename std::decay<BinaryOperation>::type>
non_commutative(BinaryOperation&& op) noexcept
{
  return non_commutative_wrapper<
          typename std::decay<BinaryOperation>::type
         >{std::forward<BinaryOperation>(op)};
}


//////////////////////////////////////////////////////////////////////
/// @brief Declares function @p f as non-commutative.
///
/// @see is_non_commutative
/// @related non_commutative_wrapper
/// @ingroup ARMITags
//////////////////////////////////////////////////////////////////////
template<typename R, typename T1, typename T2>
constexpr non_commutative_wrapper<R(T1, T2)>
non_commutative(R (*f)(T1, T2)) noexcept
{
  return non_commutative_wrapper<R(T1, T2)>{f};
}


//////////////////////////////////////////////////////////////////////
/// @brief Detects if @p BinaryOperation is non-commutative.
///
/// @ingroup ARMITypeTraitsImpl
//////////////////////////////////////////////////////////////////////
template<typename BinaryOperation>
struct is_non_commutative_impl
: public std::false_type
{ };


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref is_non_commutative_impl for non-commutative
///        operators.
///
/// @ingroup ARMITypeTraitsImpl
//////////////////////////////////////////////////////////////////////
template<typename... T>
struct is_non_commutative_impl<non_commutative_wrapper<T...>>
: public std::true_type
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Detects if @p BinaryOperation is a non-commutative operation.
///
/// @ingroup ARMITypeTraits
//////////////////////////////////////////////////////////////////////
template<typename BinaryOperation>
struct is_non_commutative
: public is_non_commutative_impl<
           typename std::remove_cv<BinaryOperation>::type
         >
{ };

} // namespace stapl

#endif
