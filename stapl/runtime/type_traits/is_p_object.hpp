/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_TYPE_TRAITS_IS_P_OBJECT_HPP
#define STAPL_RUNTIME_TYPE_TRAITS_IS_P_OBJECT_HPP

#include <type_traits>

namespace stapl {

class p_object;


////////////////////////////////////////////////////////////////////
/// @brief Detects if @p T is a @ref p_object.
///
/// @see p_object
/// @ingroup ARMITypeTraits
///
/// @todo Use @c object_traits class to detect if @p T is a @ref p_object.
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_p_object
: public std::is_base_of<p_object, typename std::remove_cv<T>::type>
{ };



////////////////////////////////////////////////////////////////////
/// @brief Detects if @p T is a reference to a @ref p_object.
///
/// @see p_object
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_p_object_reference
: public std::false_type
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref is_p_object_reference for references.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_p_object_reference<T&>
: public is_p_object<T>::type
{ };



////////////////////////////////////////////////////////////////////
/// @brief Detects if @p T is a pointer to a @ref p_object.
///
/// @see p_object
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_p_object_pointer
: public std::false_type
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref is_p_object_pointer for pointers.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_p_object_pointer<T*>
: public is_p_object<T>::type
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref is_p_object_pointer for @c const pointers.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_p_object_pointer<T* const>
: public is_p_object<T>::type
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref is_p_object_pointer for @c volatile pointers.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_p_object_pointer<T* volatile>
: public is_p_object<T>::type
{ };


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref is_p_object_pointer for @c const @c volatile
///        pointers.
///
/// @ingroup ARMITypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_p_object_pointer<T* const volatile>
: public is_p_object<T>::type
{ };

} //  namespace stapl

#endif
