/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_CONFIG_TYPES_HPP
#define STAPL_RUNTIME_CONFIG_TYPES_HPP

#include <cstdint>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Process id type.
///
/// @ingroup ARMI
//////////////////////////////////////////////////////////////////////
typedef int process_id;


//////////////////////////////////////////////////////////////////////
/// @brief Level type.
///
/// @ingroup ARMI
//////////////////////////////////////////////////////////////////////
typedef std::uint8_t level_type;


namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Location id type.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
typedef std::uint32_t location_id;


//////////////////////////////////////////////////////////////////////
/// @brief Gang id type.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
typedef std::uint32_t gang_id;


//////////////////////////////////////////////////////////////////////
/// @brief RMI nesting level type.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
typedef std::uint16_t nesting_level;


//////////////////////////////////////////////////////////////////////
/// @brief Magic number that disambiguates between different deeply-nested RMIs.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
typedef std::uint8_t magic_id;


//////////////////////////////////////////////////////////////////////
/// @brief Raw affinity tag type.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
typedef std::uint32_t internal_affinity_tag;


//////////////////////////////////////////////////////////////////////
/// @brief Registered object id type.
///
/// @warning This type has to always be able to store a pointer.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
typedef std::uintptr_t object_id;


//////////////////////////////////////////////////////////////////////
/// @brief Collective operation id type.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
typedef object_id collective_id;

} // namespace runtime

} // namespace stapl

#endif
