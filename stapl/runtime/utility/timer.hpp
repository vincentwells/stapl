/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_UTILITY_TIMER_HPP
#define STAPL_RUNTIME_UTILITY_TIMER_HPP

#include <chrono>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Provides a timer mechanism.
///
/// The caller has to start the timer and check at regular intervals if it
/// expired. It is assumed that it has milliseconds accuracy but it is not
/// guaranteed that it will expire at the exact timeout.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
template<typename Clock = std::chrono::high_resolution_clock>
class timer
{
public:
  using duration   = std::chrono::milliseconds;
  using time_point = typename Clock::time_point;

private:
  duration   m_timeout;
  time_point m_timestamp;

public:
  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the current time.
  //////////////////////////////////////////////////////////////////////
  static time_point now(void) noexcept
  { return Clock::now(); }

  explicit timer(const duration t = duration::zero()) noexcept
  : m_timeout(t)
  { }

  void reset(void) noexcept
  { m_timestamp = now(); }

  //////////////////////////////////////////////////////////////////////
  /// @brief Resets this @ref timer and sets a different timeout.
  //////////////////////////////////////////////////////////////////////
  void reset(const duration t) noexcept
  {
    m_timeout   = t;
    m_timestamp = now();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns if the @ref timer is expired.
  //////////////////////////////////////////////////////////////////////
  bool expired(const time_point t = now()) const noexcept
  {
    const auto diff = (t - m_timestamp);
    return (m_timeout <= diff);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the time until <tt>timer::expired()==true</tt> in
  ///        milliseconds.
  //////////////////////////////////////////////////////////////////////
  duration remaining(void) const noexcept
  {
    const auto t    = now();
    const auto diff = (t - m_timestamp);
    return std::chrono::duration_cast<duration>(m_timeout - diff);
   }
};

} // namespace runtime

} // namespace stapl

#endif
