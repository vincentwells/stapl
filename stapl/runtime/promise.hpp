/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_PROMISE_HPP
#define STAPL_RUNTIME_PROMISE_HPP

#include "aggregator.hpp"
#include "future.hpp"
#include "runqueue.hpp"
#include "value_handle.hpp"
#include "non_rmi/response.hpp"
#include <tuple>
#include <utility>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Provides a means of setting a result asynchronously, which may be
///        retrieved through an instance of @ref future.
///
/// @tparam R Returned object type.
///
/// @see future
/// @ingroup ARMIUtilities
///
/// @todo Type required in member types to avoid packing the pointer.
/// @todo Delete copy constructor.
/// @todo Handle the case of creating a promise and not using it.
/// @todo Handle the case of calling @ref get_future() multiple times.
/// @todo Handle broken promises.
//////////////////////////////////////////////////////////////////////
template<typename R>
class promise
{
public:
  typedef std::tuple</* T*, */process_id> member_types;
private:
  typedef runtime::value_handle<R>        handle_type;

  handle_type* m_handle;
  process_id   m_pid;

public:
  promise(void)
  : m_handle(new handle_type),
    m_pid(runtime::runqueue::get_process_id())
  { }

#if 0
  // see @todo for copy constructor
  promise(promise const&) = delete;
#else
  promise(promise const&) noexcept = default;
#endif
  promise& operator=(promise const&) = delete;

  promise(promise&& other) noexcept
  : m_handle(other.m_handle),
    m_pid(other.m_pid)
  {
    other.m_handle = nullptr;
    other.m_pid    = invalid_process_id;
  }

  promise& operator=(promise&& other) noexcept
  {
    delete m_handle;
    m_handle       = other.m_handle;
    m_pid          = other.m_pid;
    other.m_handle = nullptr;
    other.m_pid    = invalid_process_id;
    return *this;
  }

  ////////////////////////////////////////////////////////////////////
  /// @brief Returns a @ref future associated with the promised result.
  ////////////////////////////////////////////////////////////////////
  future<R> get_future(void)
  { return future<R>{std::unique_ptr<handle_type>{m_handle}}; }

  ////////////////////////////////////////////////////////////////////
  /// @brief Stores the value and makes the promise ready.
  ////////////////////////////////////////////////////////////////////
  void set_value(R const& value)
  {
    typedef runtime::response<handle_type> response_type;
    response_type r{*m_handle};
    r(m_pid, value);
  }

  ////////////////////////////////////////////////////////////////////
  /// @brief Stores the value and makes the promise ready.
  ////////////////////////////////////////////////////////////////////
  void set_value(R&& value)
  {
    typedef runtime::response<handle_type> response_type;
    response_type r{*m_handle};
    r(m_pid, std::move(value));
  }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref promise for @c void.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<>
class promise<void>
{
public:
  typedef std::tuple</* T*, */process_id> member_types;
private:
  typedef runtime::value_handle<void>     handle_type;

  handle_type* m_handle;
  process_id   m_pid;

public:
  promise(void)
  : m_handle(new handle_type),
    m_pid(runtime::runqueue::get_process_id())
  { }

#if 0
  promise(promise const&) = delete;
#else
  promise(promise const&) noexcept = default;
#endif
  promise& operator=(promise const&) = delete;

  promise(promise&& other) noexcept
  : m_handle(other.m_handle),
    m_pid(other.m_pid)
  {
    other.m_handle = nullptr;
    other.m_pid    = invalid_process_id;
  }

  promise& operator=(promise&& other) noexcept
  {
    delete m_handle;
    m_handle       = other.m_handle;
    m_pid          = other.m_pid;
    other.m_handle = nullptr;
    other.m_pid    = invalid_process_id;
    return *this;
  }

  future<void> get_future(void)
  { return future<void>{std::unique_ptr<handle_type>{m_handle}}; }

  void set_value(void)
  {
    typedef runtime::response<handle_type> response_type;
    response_type r{*m_handle};
    r(m_pid);
  }
};

} // namespace stapl

#endif
