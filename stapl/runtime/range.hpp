/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_RANGE_HPP
#define STAPL_RUNTIME_RANGE_HPP

#include "serialization.hpp"
#include "type_traits/is_movable.hpp"
#include "type_traits/is_contiguous_iterator.hpp"
#include <iterator>
#include <memory>
#include <type_traits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Describes a range of @c const objects.
///
/// @tparam Iterator Container iterator type.
///
/// This range is used to that only a part of a container needs to be
/// communicated.
///
/// @see immutable_range_wrapper
/// @ingroup ARMIUtilities
///
/// @todo Ranges over non-contiguous containers are not yet supported.
//////////////////////////////////////////////////////////////////////
template<typename Iterator, typename = void>
class range_wrapper;


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref range_wrapper for contiguous iterators.
///
/// @see is_contiguous_iterator
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename Iterator>
class range_wrapper<Iterator,
                    typename std::enable_if<
                      runtime::is_contiguous_iterator<Iterator>::value
                    >::type>
{
public:
  typedef typename std::iterator_traits<Iterator>::value_type value_type;
  typedef std::size_t                                         size_type;
  typedef value_type const*                                   const_iterator;

private:
  const size_type   m_size;
  value_type const* m_p;

public:
  range_wrapper(Iterator first, Iterator last) noexcept
  : m_size(std::distance(first, last)),
    m_p(std::addressof(*first))
  { }

  range_wrapper(Iterator first, const std::size_t size) noexcept
  : m_size(size),
    m_p(std::addressof(*first))
  { }

  const_iterator begin(void) const noexcept
  { return const_iterator{m_p}; }

  const_iterator end(void) const noexcept
  { return const_iterator{m_p + m_size}; }

  const_iterator cbegin(void) const noexcept
  { return begin(); }

  const_iterator cend(void) const noexcept
  { return end(); }

  bool empty(void) const noexcept
  { return (m_size==0); }

  size_type size(void) const noexcept
  { return m_size; }

  void define_type(typer& t)
  {
    t.member(m_size);
    t.member(m_p, m_size);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Creates a wrapper over the range <tt>[first, last)</tt>.
///
/// Upon communication, a copy of the elements in the range is given to the
/// callee.
///
/// @see make_immutable_range
/// @related range_wrapper
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename InputIterator>
range_wrapper<InputIterator>
make_range(InputIterator first, InputIterator last) noexcept
{
  return range_wrapper<InputIterator>{first, last};
}


//////////////////////////////////////////////////////////////////////
/// @brief Creates a wrapper over the range <tt>[first, first + n)</tt>.
///
/// Upon communication, a copy of the elements in the range is given to the
/// callee.
///
/// @see make_immutable_range_n
/// @related range_wrapper
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename InputIterator, typename Size>
range_wrapper<InputIterator>
make_range_n(InputIterator first, const Size count) noexcept
{
  return range_wrapper<InputIterator>(first, count);
}


namespace runtime {

////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref is_movable for @c range_wrapper.
///
/// A @ref range_wrapper objects is considered non-movable so that in shared
/// memory, the range of objects is always copied instead of passed by
/// reference.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename Iterator>
struct is_movable<range_wrapper<Iterator>>
: public std::false_type
{ };

} // namespace runtime

} // namespace stapl

#endif
