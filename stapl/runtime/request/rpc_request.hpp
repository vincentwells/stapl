/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_REQUEST_RPC_REQUEST_HPP
#define STAPL_RUNTIME_REQUEST_RPC_REQUEST_HPP

#include "../message.hpp"
#include "../exception.hpp"
#include <cstddef>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Encapsulates an RPC request directed to a process for subsequent
///        execution via the function operator.
///
/// @ref rpc_request objects package all information for buffering and transfer.
/// The 'header' contains the size of the request. The 'body' (derived class)
/// has the desired function and any arguments required to invoke it.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
class rpc_request
{
public:
  typedef std::size_t size_type;

private:
  size_type m_size;

public:
  constexpr explicit rpc_request(const size_type size) noexcept
  : m_size(size)
  { }

  rpc_request(rpc_request const&) = delete;
  rpc_request& operator=(rpc_request const&) = delete;

protected:
  ~rpc_request(void) = default;

public:
  constexpr size_type size(void) const noexcept
  { return m_size; }

  size_type& size(void) noexcept
  { return m_size; }

  virtual void operator()(message_shared_ptr&) = 0;
};


//////////////////////////////////////////////////////////////////////
/// @brief Executes RPC requests for a process.
///
/// @ingroup requestExecution
//////////////////////////////////////////////////////////////////////
struct rpc_executor
{
  void operator()(message_ptr m) const
  {
    STAPL_RUNTIME_ASSERT(m->type()==header::RPC);
    message_shared_ptr sm{std::move(m)};
    message::payload_range_type payload = sm->payload();
    do {
      auto& req = *reinterpret_cast<rpc_request*>(payload.begin());
      payload.advance_begin(req.size());
      req(sm);
    } while (!payload.empty());
  }
};

} // namespace runtime

} // namespace stapl

#endif
