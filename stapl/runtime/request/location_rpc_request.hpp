/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_REQUEST_LOCATION_RPC_REQUEST_HPP
#define STAPL_RUNTIME_REQUEST_LOCATION_RPC_REQUEST_HPP

#include "../message.hpp"
#include "../exception.hpp"
#include <cstddef>

namespace stapl {

namespace runtime {

class location_md;


//////////////////////////////////////////////////////////////////////
/// @brief Encapsulates an RPC request directed to a location for subsequent
///        execution via the function operator.
///
/// @ref location_rpc_request objects package all information for buffering and
/// transfer.
///
/// The 'header' contains the size of the request. The 'body' (derived class)
/// has the desired function and any arguments required to invoke it.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
class location_rpc_request
{
public:
  using size_type = std::size_t;

private:
  size_type m_size;

public:
  constexpr explicit location_rpc_request(const size_type size) noexcept
  : m_size(size)
  { }

  location_rpc_request(location_rpc_request const&) = delete;
  location_rpc_request& operator=(location_rpc_request const&) = delete;

protected:
  ~location_rpc_request(void) = default;

public:
  constexpr size_type size(void) const noexcept
  { return m_size; }

  size_type& size(void) noexcept
  { return m_size; }

  virtual bool operator()(location_md&, message_shared_ptr&) = 0;
};


//////////////////////////////////////////////////////////////////////
/// @brief Executes RPC requests for a specific location, preserving the
///        requests that could not be executed for a later invocation.
///
/// @ingroup requestExecution
//////////////////////////////////////////////////////////////////////
class location_rpc_executor
{
private:
  message_shared_ptr          m_msg;
  message::payload_range_type m_payload;

public:
  explicit location_rpc_executor(message_ptr m) noexcept
  : m_msg(std::move(m)),
    m_payload(m_msg->payload())
  { }

  bool operator()(location_md& l)
  {
    STAPL_RUNTIME_ASSERT((m_msg->type()==header::LOCATION_RPC) ||
                         (m_msg->type()==header::BCAST_LOCATION_RPC));
    do {
      auto& req = *reinterpret_cast<location_rpc_request*>(m_payload.begin());
      const bool done = req(l, m_msg);
      if (!done)
        return false;
      m_payload.advance_begin(req.size());
    } while (!m_payload.empty());
    m_msg.reset();
    return true;
  }
};

} // namespace runtime

} // namespace stapl

#endif
