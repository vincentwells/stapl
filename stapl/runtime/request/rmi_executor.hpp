/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_REQUEST_RMI_EXECUTOR_HPP
#define STAPL_RUNTIME_REQUEST_RMI_EXECUTOR_HPP

#include "rmi_request.hpp"
#include "../context.hpp"
#include "../message.hpp"
#include "../exception.hpp"
#include <utility>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Executes RMI requests for a specific context, preserving the requests
///        that could not be executed for a later invocation.
///
/// @ingroup requestExecution
//////////////////////////////////////////////////////////////////////
class rmi_executor
{
private:
  message_ptr                 m_msg;
  message::payload_range_type m_payload;

public:
  bool empty(void) const noexcept
  { return !m_msg; }

  bool operator()(context& ctx)
  {
    STAPL_RUNTIME_ASSERT(!empty());
    do {
      auto& req = *reinterpret_cast<rmi_request*>(m_payload.begin());
      const bool done = req(ctx);
      if (!done)
        return false;
      m_payload.advance_begin(req.size());
    } while (!m_payload.empty());
    m_msg.reset();
    ctx.count_processed();
    return true;
  }

  bool operator()(context& ctx, message_ptr m)
  {
    STAPL_RUNTIME_ASSERT(empty());
    m_msg     = std::move(m);
    m_payload = m_msg->payload();
    return operator()(ctx);
  }
};

} // namespace runtime

} // namespace stapl

#endif
