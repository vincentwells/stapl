/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_EXIT_CODE_HPP
#define STAPL_RUNTIME_EXIT_CODE_HPP

#include <cstdlib>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Class to return exit codes from STAPL applications.
///
/// A @c 0 exit code requires SPMD creation of an @ref exit_code object, whereas
/// a non @c 0 exit code does not. As such, the following uses are correct:
/// @code
/// stapl::exit_code stapl_main(int, char*[])
/// {
///   return EXIT_SUCCESS;
/// }
///
/// stapl::exit_code stapl_main(int, char*[])
/// {
///   return EXIT_FAILURE;
/// }
///
/// stapl::exit_code stapl_main(int, char*[])
/// {
///   if (condition)
///     return EXIT_FAILURE;
///   return EXIT_SUCCESS;
/// }
/// @endcode
/// whereas the following is not:
/// @code
/// stapl::exit_code stapl_main(int, char*[])
/// {
///   if (condition)
///     return EXIT_SUCCESS;
///   return EXIT_FAILURE;
/// }
/// @endcode
///
/// @warning Users should not instantiate objects of this class explicitly.
///          Instances should only be instantiated through implicit conversion
///          from @c int.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
class exit_code
{
private:
  int m_code;

public:
  exit_code(int code);

  exit_code& operator=(exit_code const&) = delete;

  //////////////////////////////////////////////////////////////////////
  /// @brief Waits for all locations to create an instance of @ref exit_code and
  ///        call @ref wait().
  ///
  /// @warning This function has to be called outside the scope the initial
  ///          @ref exit_code object was created in.
  //////////////////////////////////////////////////////////////////////
  void wait(void) const;

  int get_code(void) const noexcept
  { return m_code; }

  bool operator==(const int code) const noexcept
  { return (m_code==code); }

  bool operator!=(const int code) const noexcept
  { return !(*this==code); }
};

} // namespace stapl

#endif
