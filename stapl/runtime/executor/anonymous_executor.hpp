/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_RUNTIME_EXECUTOR_ANONYMOUS_EXECUTOR_HPP
#define STAPL_RUNTIME_EXECUTOR_ANONYMOUS_EXECUTOR_HPP

#include "../rmi_handle.hpp"

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Provides support for getting a @ref p_object on the gang of
///        @c stapl_main().
///
/// @ingroup executors
///
/// @deprecated The PARAGRAPH's use of this for out of group communication
///   has been replaced by executor_rmi.  It is used in a few places to serve
///   as a well known p_object (i.e., @ref stapl::serial_io), but this
///   usage needs to served elsewhere and this class removed.
//////////////////////////////////////////////////////////////////////
class anonymous_executor
{
private:
  rmi_handle m_handle;

public:
  //////////////////////////////////////////////////////////////////////
  /// @brief Creates a new @ref anonymous_executor and pushes it on the stack.
  //////////////////////////////////////////////////////////////////////
  anonymous_executor(void);

  //////////////////////////////////////////////////////////////////////
  /// @brief Pops the @ref anonymous_executor from the stack and destroys it.
  //////////////////////////////////////////////////////////////////////
  ~anonymous_executor(void);

  anonymous_executor(anonymous_executor const&) = delete;
  anonymous_executor& operator=(anonymous_executor const&) = delete;

  rmi_handle::const_reference const& get_rmi_handle(void) const noexcept
  { return m_handle; }

  rmi_handle::reference const& get_rmi_handle(void) noexcept
  { return m_handle; }

  unsigned int get_location_id(void) noexcept
  { return m_handle.get_location_id(); }
};


//////////////////////////////////////////////////////////////////////
/// @brief Returns the instance of the @ref anonymous_executor.
///
/// @ingroup executors
//////////////////////////////////////////////////////////////////////
anonymous_executor& get_anonymous_executor(void);

} // namespace stapl

#endif
