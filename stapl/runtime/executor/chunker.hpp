/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_RUNTIME_EXECUTOR_CHUNKER_HPP
#define STAPL_RUNTIME_EXECUTOR_CHUNKER_HPP

#include <deque>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Support class for the sliding window implementation.
///
/// @ingroup executorsImpl
//////////////////////////////////////////////////////////////////////
class chunker
{
public:
  using size_type = std::size_t;

  //////////////////////////////////////////////////////////////////////
  /// @brief Sliding window entry type.
  //////////////////////////////////////////////////////////////////////
  class entry_type
  {
  private:
    size_type m_added;
    size_type m_retired;

  public:
    constexpr entry_type(void) noexcept
    : m_added(0),
      m_retired(0)
    { }

    void add(void) noexcept
    { ++m_added; }

    void retire(void) noexcept
    { ++m_retired; }

    constexpr size_type num_added(void) const noexcept
    { return m_added; }

    constexpr bool all_retired(void) const noexcept
    { return (m_added==m_retired); }
  };

private:
  const size_type        m_max_size;
  const size_type        m_max_chunk_size;
  std::deque<entry_type> m_queue;
  size_type              m_pending;

public:
  explicit chunker(const size_type max_size, const size_type max_chunk_size)
  : m_max_size(max_size),
    m_max_chunk_size(max_chunk_size),
    m_pending(0)
  { }

  size_type max_size(void) const noexcept
  { return m_max_size; }

  size_type max_chunk_size(void) const noexcept
  { return m_max_chunk_size; }

  bool empty(void) const noexcept
  { return (m_pending==0); }

  bool full(void) const noexcept
  { return (m_pending>=m_max_size); }

  size_type pending(void) const noexcept
  { return m_pending; }

  //////////////////////////////////////////////////////////////////////
  /// @brief Return available entry.
  //////////////////////////////////////////////////////////////////////
  entry_type& get_entry(void)
  {
    ++m_pending;
    if (m_queue.empty() || (m_queue.back().num_added() == m_max_chunk_size))
      m_queue.emplace_back();
    auto& t = m_queue.back();
    t.add();
    return t;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Remove empty entries.
  //////////////////////////////////////////////////////////////////////
  void cleanup(void) noexcept
  {
    while (!m_queue.empty()) {
      auto const& chunk = m_queue.front();
      if (!chunk.all_retired())
        return;
      m_pending -= chunk.num_added();
      m_queue.pop_front();
    }
  }
};

} // namespace stapl

#endif
