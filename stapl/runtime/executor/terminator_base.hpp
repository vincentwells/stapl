/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_RUNTIME_EXECUTOR_TERMINATOR_BASE_HPP
#define STAPL_RUNTIME_EXECUTOR_TERMINATOR_BASE_HPP

#include <utility>
#include <boost/function.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Base class for termination detection.
///
/// All termination detectors must inherit from this class.
///
/// @ingroup executors
//////////////////////////////////////////////////////////////////////
class terminator_base
{
public:
  using size_type = std::size_t;
private:
  // Using boost::function instead of std::function to avoid mallocs.
  using notifier_type = boost::function<void(void)>;

  notifier_type m_notifier;

public:
  virtual ~terminator_base(void) = default;

  //////////////////////////////////////////////////////////////////////
  /// @brief Starts this terminator if required.
  //////////////////////////////////////////////////////////////////////
  virtual void operator()(void) = 0;

  //////////////////////////////////////////////////////////////////////
  /// @brief Sets the function to be called when termination is detected.
  //////////////////////////////////////////////////////////////////////
  template<typename Notifier>
  void set_notifier(Notifier&& notifier)
  { m_notifier = std::forward<Notifier>(notifier); }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the number of times the terminator has iterated.
  //////////////////////////////////////////////////////////////////////
  virtual size_type iterations(void) const noexcept = 0;

protected:
  //////////////////////////////////////////////////////////////////////
  /// @brief Calls the registered notifier when the terminator has finished.
  //////////////////////////////////////////////////////////////////////
  void call_notifier(void) const
  { m_notifier(); }
};

} // namespace stapl

#endif
