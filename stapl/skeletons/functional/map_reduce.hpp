/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_FUNCTIONAL_MAP_REDUCE_HPP
#define STAPL_SKELETONS_FUNCTIONAL_MAP_REDUCE_HPP

#include "zip_reduce.hpp"

namespace stapl {
namespace skeletons {
namespace result_of {

template<typename MapOp, typename RedOp>
using map_reduce = result_of::zip_reduce<1, MapOp, RedOp>;

} // namespace result_of

//////////////////////////////////////////////////////////////////////
/// @brief As its name implies it creates a map reduce skeleton by
/// piping the result of a @c map skeleton to a @c reduce skeleton.
///
/// @param map_op the operation to be applied in the @c map step
/// @param red_op the operation to be applied in the @c reduce step
///
/// @return a map-reduce skeleton
///
/// @ingroup skeletonsFunctional
//////////////////////////////////////////////////////////////////////
template <typename MapOp, typename RedOp>
result_of::map_reduce<MapOp, RedOp>
map_reduce(MapOp&& map_op, RedOp&& red_op)
{
  return skeletons::zip_reduce<1>(
           std::forward<MapOp>(map_op),
           std::forward<RedOp>(red_op));
}

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_FUNCTIONAL_MAP_REDUCE_HPP
