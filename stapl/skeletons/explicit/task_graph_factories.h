/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_EXPLICIT_TASK_GRAPH_FACTORIES_H
#define STAPL_SKELETONS_EXPLICIT_TASK_GRAPH_FACTORIES_H

#include <vector>
#include <utility>
#include <sstream>
#include <iterator>
#include <numeric>
#include <cmath>

#include <boost/bind.hpp>

#include <stapl/runtime.hpp>

#include <stapl/paragraph/paragraph_view.hpp>
#include <stapl/paragraph/task_factory_base.hpp>

#include <stapl/views/metadata/partitioned_mix_view_fwd.hpp>

namespace stapl {

namespace detail {

template<typename MTLRowSlice>
struct row_slice_view;

} // namespace detail

namespace composition {

template<typename T>
struct map_view;

} // namespace composition

//////////////////////////////////////////////////////////////////////
/// @brief Default implementation of a functor to choose between
/// fine-grain and coarse-grain work functions when specifying the tasks
/// of a PARAGRAPH.
///
/// @ingroup skeletonsExplicitFactoriesInternal
//////////////////////////////////////////////////////////////////////
template<typename View, bool b_coarse = false>
struct choose_wf
{
  template <typename FineWF, typename CoarseWF>
  FineWF
  operator()(FineWF const& fine_wf, CoarseWF const&) const
  {
    return fine_wf;
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Default implementation of a functor to choose between
/// fine-grain and coarse-grain work functions when specifying the tasks
/// of a PARAGRAPH.
///
/// @ingroup skeletonsExplicitFactoriesInternal
//////////////////////////////////////////////////////////////////////
template<typename View>
struct choose_wf<View, true>
{
  template <typename FineWF, typename CoarseWF>
  CoarseWF
  operator()(FineWF const&, CoarseWF const& coarse_wf) const
  {
    return coarse_wf;
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Implements the function operator to return the coarse-grained
/// work function that will be used in a task specified by a factory.
///
/// @ingroup skeletonsExplicitFactoriesInternal
//////////////////////////////////////////////////////////////////////
struct use_coarse
{
  template<typename FineWF, typename CoarseWF>
  CoarseWF
  operator()(FineWF const&, CoarseWF const& coarse_wf) const
  {
    return coarse_wf;
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Specialization for factories that receive coarsened views for which
/// they will specify tasks.
///
/// @tparam View Uncoarsened view.
/// @tparam Part Partition of uncoarsened view elements into coarse elements.
///
/// @ingroup skeletonsExplicitFactoriesInternal
//////////////////////////////////////////////////////////////////////
template<typename View, typename Part>
struct choose_wf<partitioned_mix_view<View, Part>, false>
  : public use_coarse
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Specialization for factories that receive a
/// @ref composition::map_view for which they will specify tasks.
///
/// @tparam T Uncoarsened element type of the @ref composed::map_view.
///
/// The @ref composed::map_view represents the result of a PARAGRAPH.  The
/// result is coarsened in the same way the input view to the composed map
/// was coarsened.
///
/// @ingroup skeletonsExplicitFactoriesInternal
//////////////////////////////////////////////////////////////////////
template<typename T>
struct choose_wf<composition::map_view<T> >
  : public use_coarse
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Specialization for factories that receive a
/// @ref detail::row_slice_view for which they will specify tasks.
///
/// @tparam MTLRowSlice Portion of a row of a MTL matrix.
///
/// The @ref detail::row_slice_view is used in the STAPL implementation of the
/// NAS CG benchmark.
///
/// @ingroup skeletonsExplicitFactoriesInternal
//////////////////////////////////////////////////////////////////////
template<typename MTLRowSlice>
struct choose_wf<detail::row_slice_view<MTLRowSlice> >
  : public use_coarse
{ };

} // namespace stapl

#endif
