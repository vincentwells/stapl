/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_OPERATORS_REPEAT_HPP
#define STAPL_SKELETONS_OPERATORS_REPEAT_HPP

#include <type_traits>
#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/flows/repeat_flows.hpp>
#include "repeat_impl.hpp"

namespace stapl {
namespace skeletons {
namespace result_of {

template <typename Flows, typename S, typename SizeF>
using repeat = skeletons_impl::repeat<
                 typename std::decay<S>::type,
                 typename std::decay<SizeF>::type,
                 stapl::default_type<Flows, flows::repeat_flows::piped>>;

} // namespace result_of

//////////////////////////////////////////////////////////////////////
/// @brief Repeat skeleton repeats an enclosed skeleton for the
/// lazy size provided by @c size_functor. You can see examples of
/// its usage in @c tree, @c reverse_tree, @c butterfly, etc.
///
/// @tparam Flows        the flow to be used for the @c repeat. The
///                      default flow for a repeat skeleton is piped
/// @param  skeleton     the enclosed skeleton to be repeated
/// @param  size_functor the size of a repeat skeleton is not known
///                      until the input size is known. This parameter
///                      is a functor that given the input size
///                      determines the number of iterations of this
///                      repeat skeleton
/// @return a repeat skeleton with a given lazy size functor
///
/// @see skeletons_impl::repeat
/// @see flows::repeat_flows::piped
///
/// @ingroup skeletonsOperators
//////////////////////////////////////////////////////////////////////
template <typename Flows = stapl::use_default,
          typename S, typename SizeF>
result_of::repeat<Flows, S, SizeF>
repeat(S&& skeleton, SizeF&& size_functor)
{
  return result_of::repeat<Flows, S, SizeF>(
           std::forward<S>(skeleton),
           std::forward<SizeF>(size_functor));
}

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_OPERATORS_REPEAT_HPP
