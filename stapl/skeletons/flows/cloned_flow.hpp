/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_CLONED_FLOW_HPP
#define STAPL_SKELETONS_CLONED_FLOW_HPP

#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/integer_sequence.hpp>
#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/flows/numbered_flow.hpp>

namespace stapl {
namespace skeletons {
namespace flows {

namespace result_of {

template <typename Flow, std::size_t N,
          typename Indices = stapl::make_index_sequence<N>>
struct cloned_flow;

template <typename Flow, std::size_t N, std::size_t...Indices>
struct cloned_flow<Flow, N, stapl::index_sequence<Indices...>>
{
  using type = stapl::tuple<numbered_flow<Indices, Flow>...>;
};

} // namespace result_of

namespace flows_impl {

template <typename Flow, std::size_t... Indices>
auto
cloned_flow(Flow const& flow, stapl::index_sequence<Indices...>&&)
STAPL_AUTO_RETURN((
  stapl::make_tuple(make_numbered_flow<Indices>(flow)...)
))

} // namespace flows_impl

//////////////////////////////////////////////////////////////////////
/// @brief Clones a given flow for the number of times specified
/// by @c N. A cloned flow created by this method will wrap the given
/// flow by @c numbered_flow with increasing indices starting from 0.
///
/// @tparam N   the number of times the flow should be cloned
/// @param flow a single flow to be cloned
///
/// @ingroup skeletonsFlows
//////////////////////////////////////////////////////////////////////
template <std::size_t N, typename Flow>
typename flows::result_of::cloned_flow<Flow, N>::type
cloned_flow(Flow&& flow)
{
  return flows_impl::cloned_flow(
           std::forward<Flow>(flow),
           stapl::make_index_sequence<N>());
}

} // namespace flows
} // namespace skeletons
} // namespace stapl
#endif // STAPL_SKELETONS_CLONED_FLOW_HPP
