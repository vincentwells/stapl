/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_EXECUTORS_EXECUTION_PARAMS_HPP
#define STAPL_SKELETONS_EXECUTORS_EXECUTION_PARAMS_HPP

#include <stapl/runtime/executor/scheduler/sched.hpp>
#include <stapl/views/metadata/coarseners/null.hpp>

namespace stapl {
namespace skeletons {
namespace skeletons_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Stores the execution parameters to be used for running a
/// a skeleton in STAPL. These parameters are currently:
/// @tparam ResultType the result type of the given skeleton if any.
/// @tparam isBlocking whether the execution should be blocking or not.
/// @tparam OptionalParams... the following optional parameters can
///                    be passed:
///                    1. Coarsener - data coarsener to be used
///                    2. ExtraEnv  - an extra spawning environment to
///                       be used in addition to the default @c taskgraph_env
///                    3. Scheduler - scheduler to be used for the generated
///                       taskgraph.
///
/// @ingroup skeletonsExecutors
//////////////////////////////////////////////////////////////////////
template <typename ResultType, bool isBlocking,
          typename... OptionalParams>
class execution_params
{
protected:
  using default_scheduler_type = default_scheduler;
  using default_param_types    = tuple<
                                   null_coarsener,     // coarsener
                                   stapl::use_default, // extra_env
                                   default_scheduler>; // scheduler
  using param_types            = typename compute_type_parameters<
                                   default_param_types,
                                   OptionalParams...>::type;
public:
  static constexpr bool is_blocking = isBlocking;

  using result_type    = ResultType;
  using coarsener_type = typename tuple_element<0, param_types>::type;
  using extra_env_type = typename tuple_element<1, param_types>::type;
  using scheduler_type = typename tuple_element<2, param_types>::type;

  execution_params(coarsener_type const& coarsener,
                   extra_env_type const& extra_env,
                   scheduler_type const& scheduler)
    : m_coarsener(coarsener),
      m_scheduler(scheduler),
      m_extra_env(extra_env)
  { }

  execution_params(coarsener_type const& coarsener,
                   extra_env_type const& extra_env)
    : execution_params(coarsener, extra_env, scheduler_type())
  { }

  execution_params(coarsener_type const& coarsener)
    : execution_params(coarsener, extra_env_type(), scheduler_type())
  { }

  execution_params()
    : execution_params(coarsener_type(), extra_env_type(), scheduler_type())
  { }

  coarsener_type const& coarsener() const
  { return m_coarsener; }

  scheduler_type const& scheduler() const
  { return m_scheduler; }

  extra_env_type const& extra_env() const
  { return m_extra_env; }

  void define_type(typer& t)
  {
    t.member(m_coarsener);
    t.member(m_scheduler);
    t.member(m_extra_env);
  }

private:
  coarsener_type m_coarsener;
  scheduler_type m_scheduler;
  extra_env_type m_extra_env;
};

//////////////////////////////////////////////////////////////////////
/// @brief Default execution parameters.
///
/// This class reduces the symbol size for default cases.
//////////////////////////////////////////////////////////////////////
struct default_execution_params
  : public execution_params<void, false>
{ };

}  // namespace skeletons_impl

//////////////////////////////////////////////////////////////////////
/// @brief Creates an @c execution_params instance with user provided
/// values. All the unprovided arguments will be assigned default values.
///
/// @tparam ResultType the return type of skeleton which is used in
///                    reducing skeletons such as @c reduce, @c map_reduce.
/// @tparam isBlocking whether the execution needs to be blocking or not.
/// @param OptionalParams... the following optional parameters can
///                    be passed:
///                    1. Coarsener - data coarsener to be used
///                    2. ExtraEnv  - an extra spawning environment to
///                       be used in addition to the default @c taskgraph_env
///                    3. Scheduler - scheduler to be used for the generated
///                       taskgraph.
//////////////////////////////////////////////////////////////////////
template <typename ResultType =
            skeletons_impl::default_execution_params::result_type,
          bool isBlocking     =
            skeletons_impl::default_execution_params::is_blocking,
          typename... OptionalParams>
skeletons_impl::execution_params<
  ResultType, isBlocking,
  typename std::decay<OptionalParams>::type...>
execution_params(OptionalParams&&... optional_params) {
  return skeletons_impl::execution_params<
           ResultType, isBlocking,
           typename std::decay<OptionalParams>::type...>(
             std::forward<OptionalParams>(optional_params)...);
}

//////////////////////////////////////////////////////////////////////
/// @brief Creates an @c execution_params instance with default values.
//////////////////////////////////////////////////////////////////////
inline skeletons_impl::default_execution_params
default_execution_params()
{
  return skeletons_impl::default_execution_params();
}

}  // namespace skeletons
}  // namespace stapl

#endif  // STAPL_SKELETONS_EXECUTORS_EXECUTION_PARAMS_HPP
