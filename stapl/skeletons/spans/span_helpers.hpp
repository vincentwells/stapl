/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_SPANS_SPAN_HELPERS_HPP
#define STAPL_SKELETONS_SPANS_SPAN_HELPERS_HPP

#include <stapl/algorithms/functional.hpp>
#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/utility/tuple/pad_tuple.hpp>

namespace stapl {
namespace skeletons {
namespace spans {

template <int level, typename Op, typename View, typename LevelDims>
void
compute_total_dimension_helper(
  std::false_type, View const& view, LevelDims& level_dims)
{
  using coarsener_t = typename Op::execution_params_t::coarsener_type;

  auto&& coarsened_view = std::get<0>(coarsener_t()(std::make_tuple(view)));

  constexpr size_t padded_size =
    tuple_size<typename LevelDims::value_type>::value;

  level_dims[level_dims.size() - level - 1] =
    tuple_ops::pad_tuple<padded_size>(coarsened_view.domain().dimensions(), 1);
}

/////////////////////////////////////////////////////////////////////////
/// @brief For the case that View is nested(it's elements are views too)
///        and the view is 1D.
////////////////////////////////////////////////////////////////////////
template <int level, typename Op, typename View, typename LevelDims>
void
compute_total_dimension_helper(
  std::true_type, View const& view, LevelDims& level_dims)
{
  using is_nested =
    std::integral_constant<bool, 0 != level - 1>;

  constexpr size_t padded_size =
    tuple_size<typename LevelDims::value_type>::value;

  level_dims[level_dims.size() - level - 1] =
    tuple_ops::pad_tuple<padded_size>(view.domain().dimensions(), 1);

  compute_total_dimension_helper<
    level - 1,
    typename Op::wrapped_skeleton_type::op_type>(
    is_nested(),
    view.make_reference(view.domain().first()),
    level_dims);
}


//////////////////////////////////////////////////////////////////////
/// @brief It goes through to the nested views and calculates the
///        flattened equivalent of nested dimensions. For example,
///        for a 2*2 view that each of its elements is 3*3 view
///        the total dimension would be 6*6 and level dims would a
///        vector of tuples([(2,2) , (3,3)]).
///        This information is needed for mappers and currently
///        suffices just for fine grain executions.
///
/// @param view       the input view
/// @param level_dims the variable that is used for storing
///                   the dimension of each nested variable
///
/// @ingroup skeletonsSpans
//////////////////////////////////////////////////////////////////////
template <int level, typename Op, typename View, typename LevelDims>
void
compute_total_dimension(View const& view, LevelDims& level_dims)
{
  using is_nested =
    std::integral_constant<bool, 0 != level - 1>;

  constexpr size_t padded_size =
    tuple_size<typename LevelDims::value_type>::value;

  level_dims[level_dims.size() - level - 1] =
    tuple_ops::pad_tuple<padded_size>(view.domain().dimensions(), 1);

  compute_total_dimension_helper<level - 1, Op>(
    is_nested(),
    view.make_reference(view.domain().first()),
    level_dims);
}


} // namespace spans
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_SPANS_SPAN_HELPERS_HPP
