/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PARAGRAPH_FAST_VIEW_HPP
#define STAPL_PARAGRAPH_FAST_VIEW_HPP

#include <boost/mpl/has_xxx.hpp>

namespace stapl {

BOOST_MPL_HAS_XXX_TRAIT_DEF(fast_view_type)

//////////////////////////////////////////////////////////////////////
/// @brief Computes view type used if view localization (i.e. is_local())
///   succeeds during PARAGRAPH task creation.
/// @ingroup pgViewOps
///
/// @param View Type of view passed to PARAGRAPH at task creation (@p add_task)
///
/// Reflects @p View::fast_view_type if definition exists.  Otherwise,
/// @p View itself is returned (signifying no localization transformation
/// exists to apply).
//////////////////////////////////////////////////////////////////////
template <typename View, bool = has_fast_view_type<View>::value>
struct get_fast_view_type
{
  typedef typename View::fast_view_type type;
};


template <typename View>
struct get_fast_view_type<View, false>
{
  typedef View type;
};

} // namespace stapl

#endif // STAPL_PARAGRAPH_FAST_VIEW_HPP
